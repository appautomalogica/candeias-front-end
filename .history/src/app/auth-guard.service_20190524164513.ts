import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { Http, Response, Headers, ResponseContentType, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: NbAuthService,
    private http: Http,
    private router: Router) {
  }

  public nomeUser = '';

  ngOnInit() {
  }

  canActivate(): Observable<boolean> {

    let currentUser = JSON.parse(localStorage.getItem('candeias'));

    console.log("Auth")
     console.log(currentUser)
    // console.log(currentUser.user)
    // console.log(currentUser.pass)

    if (currentUser === null) {
      this.router.navigate(['/auth'])
    } else {
      // return this.getLogin('user', 'pass')
      return this.getLogin(currentUser.user, currentUser.pass)
        .map(
        (res: Response) => {
          console.log("linha 73")
          console.log(res.json());
          if (res.json()) {
            console.log("logou")
            this.nomeUser = currentUser.user;
            localStorage.setItem('candeiasErro', JSON.stringify(false));
            return true
          } else {
            console.log("nao logou")
            localStorage.removeItem('candeias');
            localStorage.clear();
            localStorage.setItem('candeiasErro', JSON.stringify(true));
            this.router.navigate(['/auth'])
            //return false
          }
        })
    }
    // .take(1) //
  }


  public getLogin(user, pass): any {
    console.log("entrou na função get login")
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    console.log("linha 95")
    return this.http.post('http://cec-termoeletrica.ddns.net:81/auth', (enviar), {
    // return this.http.post('http://localhost:81/auth', (enviar), {
      headers: headers,
      method: "POST"
    });
  }
}
