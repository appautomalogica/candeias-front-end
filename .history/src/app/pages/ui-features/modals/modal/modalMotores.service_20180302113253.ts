import { Injectable } from '@angular/core';
import { Http, Response, Headers, ResponseContentType, RequestOptions } from '@angular/http';
import * as moment from 'moment';
import 'moment/locale/pt-br';

@Injectable()
export class MotorRealTimeService {

  public v;
  public pottot;
  public horimetro;
  public estado;
  public nome;
  public grupo;
  public id;
  public isMobile;
  

  constructor(
    private http: Http,
  ) {

    // let umMesEmHoras = 30 * 24;
    // this.getValoresEpmRest('CAN_GI_M64_DJ_PotAt', umMesEmHoras).subscribe(
    //   data => {
    //     this.processaValoresRecebidosSQLparaGraficosNovo(data);
    //   },
    //   err => console.log(err)
    // );

    // let pnEstados = 'CAN_GI_M55_DJ_Msinc,CAN_GI_M55_RST_Indisp,CAN_GI_M55_RST_Manut,CAN_GI_M55_RST_MotRst';

    // this.getValoresEpmRest(pnEstados, umMesEmHoras).subscribe(
    //   data => {
    //     this.processaValoresRecebidosSQLparaGraficosNovo(data);
    //   },
    //   err => console.log(err)
    // );

  }

  dadosGrafPotMotor = [
    {
      name: 'Total',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
  ];

  dadosGrafDisp = [
    {
      name: 'Total',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
  ];

  public getValoresEpmRest(mot, tempoH): any {
    let enviar = { motores: mot, horas: tempoH };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('/api/epmHistPotenciaMotorEspecifico', (enviar), {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => {
        // console.log("JSON RESPOSTA::: ");
        // console.log(res);
        return res.json();
      }
    );
  }

  public processaValoresRecebidosSQLparaGraficosNovo(v: any): any {

    function unique(arr, prop) {
      return arr.map(function (e) { return e[prop]; }).filter(function (e, i, a) {
        return i === a.indexOf(e);
      });
    }
    function Right(str, n) {
      if (n <= 0)
        return "";
      else if (n > String(str).length)
        return str;
      else {
        var iLen = String(str).length;
        return String(str).substring(iLen, iLen - n);
      }
    }
    interface grafico {
      name: string;
      series: any;
    }

    let pathnames = unique(v, 'nome');

    var ret: { [id: string]: grafico; } = {};

    for (var i = 0, len = pathnames.length; i < len; ++i) {
      let n = pathnames[i];
      let tipo;
      tipo = "_DJ_PotAt";
      if (Right(pathnames[i], tipo.length) == tipo) {
        let vn = n.split("_");
        n = vn[1] + " - " + vn[2];
        n = n.replace("G", "Grupo ");
        n = n.replace("M", "Motor ");
      }
      tipo = "_DJ_Msinc";
      if (Right(pathnames[i], tipo.length) == tipo) {
        n = "Gerando";
      }
      tipo = "_RST_Indisp";
      if (Right(pathnames[i], tipo.length) == tipo) {
        n = "Indisponível";
      }
      tipo = "_RST_Manut";
      if (Right(pathnames[i], tipo.length) == tipo) {
        n = "Em manutenção"
      }
      tipo = "_RST_MotRst";
      if (Right(pathnames[i], tipo.length) == tipo) {
        n = "Com restrição";
      }

      console.log("pathnames[0]")
      console.log(pathnames[i] + " n = "+n)
      ret[pathnames[i]] = { name: n, series: [] };
    }


    for (var i = 0, len = v.length; i < len; ++i) {
      const d: { name, value } = { name: '', value: '' };
      d.name = (moment(v[i].ts).toDate());
      if (v[i].valor == null) {
        d.value = 0;
      } else {
        d.value = Math.round(v[i].valor);
      }
      ret[v[i].nome].series.push(d);
    }

    let k = Object.keys(ret);

    let retorno = [];
    for (var i = 0, len = pathnames.length; i < len; ++i) {
      retorno[i] = ret[k[i]];
    }

    return retorno;

    // if (Right(pathnames[0], "_DJ_PotAt".length) == "_DJ_PotAt") {
    //   for (var i = 0, len = pathnames.length; i < len; ++i) {
    //     this.dadosGrafPotMotor[i] = ret[k[i]];
    //   }
    //   this.dadosGrafPotMotor = [...this.dadosGrafPotMotor];
    // } else {
    //   for (var i = 0, len = pathnames.length; i < len; ++i) {
    //     this.dadosGrafDisp[i] = ret[k[i]];
    //   }
    //   this.dadosGrafDisp = [...this.dadosGrafDisp];
    // }

    // this.themeSubscriptionLine = this.theme.getJsTheme().subscribe(config => {
    //   const colors: any = config.variables;
    //   this.colorSchemeLine = {
    //     domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
    //   };
    // });
  }

}