import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Dashboard',
    icon: 'nb-tables',
    link: '/pages/dashboard',
    home: true,
    // children: [
    //   {
    //     title: 'Visão geral',
    //     link:  '/pages/dashboard',
    //   },{
    //     title: 'Motores',
    //     link: '/pages/tables/motors',
    //   },
    // ],
  },
  // {
  //   title: 'Usinas',
  //   icon: 'nb-gear',
  //   link: '/pages/usinas',
  // },
  {
    title: 'Global 1',
    icon: 'nb-grid-a',
    link: '/pages/global1',
  },
  {
    title: 'Global 2',
    icon: 'nb-grid-a',
    link: '/pages/global2',
  },
  {
    title: 'Motores',
    icon: 'nb-gear',
    link: '/pages/tables/motors',
    // children: [
    //   {
    //     title: 'Visão geral',
    //     link:  '/pages/dashboard',
    //   },{
    //     title: 'Motores',
    //     link: '/pages/tables/motors',
    //   },
    // ],
  },

/*
  {
    title: 'FEATURES',
    group: true,
  },
*/

  /*
  {
    title: 'UI Features',
    icon: 'nb-keypad',
    link: '/pages/ui-features',
    children: [
      {
        title: 'Buttons',
        link: '/pages/ui-features/buttons',
      },
      {
        title: 'Grid',
        link: '/pages/ui-features/grid',
      },
      {
        title: 'Icons',
        link: '/pages/ui-features/icons',
      },
      {
        title: 'Modals',
        link: '/pages/ui-features/modals',
      },
      {
        title: 'Typography',
        link: '/pages/ui-features/typography',
      },
      {
        title: 'Animated Searches',
        link: '/pages/ui-features/search-fields',
      },
      {
        title: 'Tabs',
        link: '/pages/ui-features/tabs',
      },
    ],
  },
  {
    title: 'Forms',
    icon: 'nb-compose',
    children: [
      {
        title: 'Form Inputs',
        link: '/pages/forms/inputs',
      },
      {
        title: 'Form Layouts',
        link: '/pages/forms/layouts',
      },
    ],
  },
  {
    title: 'Components',
    icon: 'nb-gear',
    children: [
      {
        title: 'Tree',
        link: '/pages/components/tree',
      }, {
        title: 'Notifications',
        link: '/pages/components/notifications',
      },
    ],
  },
  {
    title: 'Maps',
    icon: 'nb-location',
    children: [
      {
        title: 'Google Maps',
        link: '/pages/maps/gmaps',
      },
      {
        title: 'Leaflet Maps',
        link: '/pages/maps/leaflet',
      },
      {
        title: 'Bubble Maps',
        link: '/pages/maps/bubble',
      },
    ],
  },
  {
    title: 'Editors',
    icon: 'nb-title',
    children: [
      {
        title: 'TinyMCE',
        link: '/pages/editors/tinymce',
      },
      {
        title: 'CKEditor',
        link: '/pages/editors/ckeditor',
      },
    ],
  },
  {
    title: 'Gráficos',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Echarts',
        link: '/pages/charts/echarts',
      },
      {
        title: 'Charts.js',
        link: '/pages/charts/chartjs',
      },
      {
        title: 'D3',
        link: '/pages/charts/d3',
      },
    ],
  },
  */
  {
    title: 'Relatórios',
    icon: 'nb-compose',
    link: '/pages/tables/reports',
    // children: [
    //   {
    //     title: 'Elipse Power',
    //     link: '/pages/tables/smart-table',
    //   },
    // ],
  },
  {
    title: 'Cadastro de usuários',
    icon: 'nb-person',
    link: '/pages/cadastrousuarios',
    // children: [
    //   {
    //     title: 'Elipse Power',
    //     link: '/pages/tables/smart-table',
    //   },
    // ],
  },

  // {
  //   title: 'Notificações',
  //   icon: 'nb-email',
  //   link: '/pages/notifications',
  //   // icon: 'ion-alert',
  //   // children: [
  //   //   {
  //   //     title: 'Elipse Power',
  //   //     link: '/pages/tables/smart-table',
  //   //   },
  //   // ],
  // },
  // {
  //   title: 'Autenticação',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Registro',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
