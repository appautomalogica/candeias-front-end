import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Global1Component } from './dashboard/global1.component';
import { Global2Component } from './dashboard/global2.component';
import { NotificacoesComponent } from './dashboard/notificacoes.component';
import { CadastroUsuariosComponent } from './dashboard/cadastro-usuarios.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  },
  // {
  //   path: 'usinas',
  //   component: UsinasComponent,
  // },
  {
    path: 'global1',
    component: Global1Component,
  },
  {
    path: 'global2',
    component: Global2Component,
  },
  {
    path: 'notifications',
    component: NotificacoesComponent,
  },
  {
    path: 'cadastrousuarios',
    component: CadastroUsuariosComponent,
  },
  {
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  },
  // {
  //   path: 'maps',
  //   loadChildren: './maps/maps.module#MapsModule',
  // },
  {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  },
  // {
  //   path: 'editors',
  //   loadChildren: './editors/editors.module#EditorsModule',
  // },
  // {
  //   path: 'forms',
  //   loadChildren: './forms/forms.module#FormsModule',
  // },
  {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  },
  // {
  //   path: 'login',
  //   loadChildren: './login/login.module#LoginModule',
  // },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
