import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

//import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Http, Response, Headers, ResponseContentType, RequestOptions } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import * as moment from 'moment';
import 'moment/locale/pt-br';

import * as FileSaver from 'file-saver';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './smart-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class SmartTableComponent {

  settings = {
    noDataMessage : "",
    actions:{
      add : false,
      edit : false,
      delete : false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      nome: {
        title: 'Relatório',
        type: 'string',
        class: '',
        width: '60%',
        filter: {
          type: 'list',
          config: {
            selectText: '',
            list: [
              { value: 'Relatório de alarmes e eventos'   					, title: 'Relatório de alarmes e eventos'   					},
              { value: 'Relatório de controle de estoque'   				, title: 'Relatório de controle de estoque'   					},
              { value: 'Relatório de nível de combustível e lubrificante'   , title: 'Relatório de nível de combustível e lubrificante'		},
              { value: 'Relatório de rampas de partida'   					, title: 'Relatório de rampas de partida'   					},
              { value: 'Relatório de rampas de parada'   					, title: 'Relatório de rampas de parada'   						},
              { value: 'Relatório de turno'						, title: 'Relatório de turno'							},
              { value: 'Relatórios de meio ambiente'						, title: 'Relatórios de meio ambiente'							},
            ],
          },
        },
      },
      data: {
        title: 'Data',
        type: 'Date',
        compareFunction(dir?: any, value1?: any, value2?: any) {
          let formato = "LLL";
          //console.log(dir + " | " + moment(value1,formato) + " ---- " + moment(value2,formato) + " = " + moment(value1,formato).diff( moment(value2,formato), 'seconds')  );
          if (dir == -1)
            return moment(value2,formato).diff( moment(value1,formato), 'seconds') ;
          else
            return moment(value1,formato).diff( moment(value2,formato), 'seconds') ;
          },

      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    // private service: SmartTableService,
            private http : Http,
            ) {



    //const data = this.service.getData();
    //this.source.load(data);
    //console.log("input table:");
    //console.log(data);
    //console.log(JSON.stringify(data));


    this.getLista('user1','user1_secret').subscribe(
             data=> {
                this.montaLista(data);
                //console.log(data);
             },
               err=> console.log(err)
             );
/*
    this.getTabela('user1','user1_secret').subscribe(
    data=> {
        //this.montaLista(data);
        console.log("Recebeu dados tabela:");
        console.log(data);
    },
      err=> console.log(err)
    );
*/

    //this.source.load(y);

    /*
    this.http.get('http://localhost:81/login/user1').subscribe(
      (r:Response) =>{

                    this.http.get('http://localhost:81/lista').subscribe(
                      (res: Response) =>{
                        console.log(res.json());
                      }
                    );
      }
    );
    */

  }

  montaLista(data){
    // data=
    // ["REventos_2017119_1400.pdf", "REventos_2017119_1401.pdf", "REventos_2017119_1402.pdf", "REventos_2017119_1403.pdf", "REventos_2017119_1404.pdf", "REventos_2017119_1405.pdf"]
    let lista = [];
    for (var i = 0; i < data.length; i++) {
       //console.log(data[i]);
       let nomeArquivo = data[i].split('.pdf')[0];
       //console.log("nome:" + nomeArquivo );
       let nomeRelatorio = data[i].split('_')[0];
       //console.log("nome relatorio:" + nomeRelatorio );
       let dataFormatada = data[i].split('_')[1]+ " " + data[i].split('_')[2];
       //console.log(dataFormatada);
       let nomeExibicaoRelatorio = nomeRelatorio;

       if(nomeRelatorio === "Relatório"){
         let nomeRelatorio1 = data[i].split('_')[1];
         if(nomeRelatorio1 === "Operacional"){
          nomeExibicaoRelatorio = "Relatório de turno";
          let hora =  data[i].split('_')[3];
          let horaFormatada =  data[i].split('-')[1];
          dataFormatada = data[i].split('_')[2]+ " " + horaFormatada;
         }
       }

       if(nomeRelatorio === "RCombLub"){
        nomeExibicaoRelatorio = "Relatório de nível de combustível e lubrificante";
       }
       if(nomeRelatorio === "RCtrlEstoque"){
        nomeExibicaoRelatorio = "Relatório de controle de estoque";
       }
       if(nomeRelatorio === "REventos"){
        nomeExibicaoRelatorio = "Relatório de alarmes e eventos";
       }
       if(nomeRelatorio === "RRampaPrd"){
        nomeExibicaoRelatorio = "Relatório de rampas de parada";
       }
       if(nomeRelatorio === "RRampaPrt"){
        nomeExibicaoRelatorio = "Relatório de rampas de partida";
       }

       if(nomeRelatorio === "Global1_RelatorioCalibracao"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global1_RelatorioProbeA"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global1_RelatorioProbeB"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global1_RelatorioProbeC"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global1_RelatorioProbeD"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }

       if(nomeRelatorio === "Global2_RelatorioCalibracao"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global2_RelatorioProbeA"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global2_RelatorioProbeB"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global2_RelatorioProbeC"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }
       if(nomeRelatorio === "Global2_RelatorioProbeD"){
        nomeExibicaoRelatorio = "Relatórios de meio ambiente";
       }

       let obj =
      {
        "nome": nomeExibicaoRelatorio,
        "data": moment(dataFormatada, 'YYYYMMD hmm').format('LLL') ,
        "attr.id" : data[i]
      };
      lista.push(obj);
    }
    this.source.load(lista);
//    this.source.setSort([{ field: 'data', direction: 'desc' }]);
    this.source.setSort([{ field: 'data', direction: 'desc',
    compare: (dir: any, value1: any, value2: any) => {
        let formato = "LLL";
        //console.log(dir + " | " + moment(value1,formato) + " ---- " + moment(value2,formato) + " = " + moment(value1,formato).diff( moment(value2,formato), 'seconds')  );
        if (dir == -1)
          return moment(value2,formato).diff( moment(value1,formato), 'seconds') ;
        else
          return moment(value1,formato).diff( moment(value2,formato), 'seconds') ;
    }
   }]);
  }

  public getLista(user,pass): any{
    let enviar = { username: user, password: pass  };
     let headers = new Headers({ 'Content-Type' : 'application/json' });
     return this.http.post('http://cec-termoeletrica.ddns.net/api/lista', (enviar), {
      headers: headers,
      method:"POST"
    }).map(
      (res:Response) => {
          //console.log("JSON RESPOSTA::: "+res);
          return res.json();
        }
    );
 }


 public getTabela(user,pass): any{
  let enviar = { username: user, password: pass  };
   let headers = new Headers({ 'Content-Type' : 'application/*' });
   return this.http.post('/api/epm', (enviar), {
    headers: headers,
    method:"POST"
  }).map(
    (res:Response) => {
        //console.log("JSON RESPOSTA::: "+res);
        let v= res.json();
        for(var i = 0, len = v.length; i < len; ++i) {
          console.log(v[i]);
        }
        return v;
      }
  );
}



 public downloadFile(user,pass, arq): any{
  let enviar = { username: user, password: pass  };
   let headers = new Headers({ 'Content-Type' : 'application/json', 'Accept': 'application/pdf' });
   return this.http.post('/api/Relatorios1/'+arq, (enviar), {
    headers: headers,
    method:"POST",
    responseType:ResponseContentType.Blob
  })
  .subscribe(
    data => this.downloadFilePDF(data,arq),
    error => alert("Error downloading file!"),
    () => console.log("OK!")
  );
}

downloadFilePDF(data: Response,filename){

  let fileBlob = data.blob();
  let blob = new Blob([fileBlob], {
     type: 'application/pdf' // must match the Accept type
  });
  FileSaver.saveAs(blob, filename);

}

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onUserRowSelect(event): void {
    //console.log(event);
    console.log(event.data['attr.id']);
    this.downloadFile("","",event.data['attr.id']);
    //this.dl("","",event.data['attr.id']);
  }

}
