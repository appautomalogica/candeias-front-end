import { StateService } from './../../@core/data/state.service';
import { Component } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Http, Response, Headers, ResponseContentType, RequestOptions } from '@angular/http';
//import * as echarts from 'echarts';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { forEach } from '@angular/router/src/utils/collection';
import { isNull } from 'util';

@Component({
  selector: 'ngx-notificacoes',
  styleUrls: ['./notificacoes.component.scss'],
  templateUrl: './notificacoes.component.html',
})
export class NotificacoesComponent {
  optionsHistDisp: { backgroundColor: any; color: any[]; tooltip: { trigger: string; axisPointer: { type: string; label: { backgroundColor: any; }; }; }; grid: { top: string; left: string; right: string; bottom: string; containLabel: boolean; }; xAxis: { type: string; boundaryGap: boolean; data: any; axisTick: {}; axisLine: { lineStyle: { color: any; }; }; axisLabel: { show: boolean; interval: number; rotate: number; margin: number; formatter: string; textStyle: { color: any; fontFamily: string; fontSize: number; fontStyle: string; fontWeight: string; }; }; }[]; yAxis: { name: string; type: string; axisLine: { lineStyle: { color: any; }; }; splitLine: { lineStyle: { color: any; }; }; axisLabel: { textStyle: { color: any; }; }; }[]; series: { name: string; type: string; itemStyle: { normal: { color: string; lineStyle: { width: number; }; }; emphasis: { color: string; }; }; data: any; smooth: boolean; animationDelay: (idx: any) => number; }[]; };

  v: any = [];

  numeroTotalMotores: number;
  v11: number = 0;
  v12: number = 0;
  v21: number = 0;
  v22: number = 0;
  v31: number = 0;
  v32: number = 0;
  v41: number = 0;
  v42: number = 0;

  G1numeroTotalMotores: number;
  G1v11: number = 0;
  G1v12: number = 0;
  G1v21: number = 0;
  G1v22: number = 0;
  G1v31: number = 0;
  G1v32: number = 0;
  G1v41: number = 0;
  G1v42: number = 0;

  G2numeroTotalMotores: number;
  G2v11: number = 0;
  G2v12: number = 0;
  G2v21: number = 0;
  G2v22: number = 0;
  G2v31: number = 0;
  G2v32: number = 0;
  G2v41: number = 0;
  G2v42: number = 0;

  t101: number = 0;
  t102: number = 0;
  t103: number = 0;
  t104: number = 0;
  t121: number = 0;
  t122: number = 0;
  t131: number = 0;
  t132: number = 0;
  t180: number = 0;
  t200: number = 0;

  valor: any;
  inef: any;
  indisp: any;

  valorG1: any;
  inefG1: any;
  indispG1: any;

  valorG2: any;
  inefG2: any;
  indispG2: any;

  types = ["12 horas", "1 dia", "1 semana", "15 dias"];
  type: any = "12 horas";

  status: string = "não iniciado";
  connected: boolean = false;
  //websocket: any;
  // url: string = 'ws://cec-termoeletrica.ddns.net:8765/';
  // url: string = 'ws://cec-termoeletrica.ddns.net:8181/Servidor';
  // url: string = 'ws://cec-termoeletrica.ddns.net/ws/Servidor';
  // websocket: WebSocket = new WebSocket(this.url);

  constructor(
    private http: Http,
    private theme: NbThemeService,
  ) {


    // console.log("this.multiLine")
    // console.log(this.multiLine)
    //console.log('dash start')
    //let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //console.log(currentUser)

    // this.verificaConexao();
    // setInterval(this.verificaConexao.bind(this), 1000);

    // this.updGraficos();
    // setInterval(this.updGraficos.bind(this), 30000);

    // this.updGraficosNovo();
    // setInterval(this.updGraficosNovo.bind(this), 5*60000);

    //updater.setStatus('opening');
    // this.status = "opening";

    // var url = 'ws://localhost:8765/';
    // this.connected = false;

    // this.updGraficosFator();
    // setInterval(this.updGraficosFator.bind(this), 5*60000);

    // this.updGraficosHistDisp();
    // setInterval(this.updGraficosHistDisp.bind(this), 5*60000);


    // /// GRAFICO LINHA
    // this.themeSubscriptionLine = this.theme.getJsTheme().subscribe(config => {
    //   const colors: any = config.variables;
    //   this.colorSchemeLine = {
    //     domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
    //   };
    // });

    
    let currentUser = JSON.parse(localStorage.getItem('candeias'));
    this.user = currentUser.user;
    
    this.getListaMensagens();
  }
user;




  public getTabelaFator(fator, pathname): any {
    let enviar = { 'fator': fator, 'pathname': pathname };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('/api/epmFator', (enviar), {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => {
        //console.log("JSON RESPOSTA::: "+res);
        return res.json();
      }
      );
  }
  addToLastValues(obj: any): any {

    let v = this.v;
    Object.keys(obj).forEach(function (key) {

      //console.log(key, obj[key]);
      v[key] = obj[key].v;
    });
    this.v = v;
  }

  private array2dict(a) {
    let d = [];
    for (var i = 0, len = a.length; i < len; ++i) {
      let p = a[i].p;
      d[p] = a[i].v;
    }
    return d;
  }



  public processaValoresRecebidosSQLparaGraficosHist(v: any): any {
    let x = [];
    let y = [];
    for (var i = 0, len = v.length; i < len; ++i) {
      //console.log(v[i].pot + " | "+ moment(v[i].ts).format('LLL'))
      x.push(moment(v[i].ts).format('D/M, H') + 'h');
      if (v[i].valor == null) {
        y.push(0);
      } else {
        y.push(Math.round(v[i].valor));
      }
    }
    x.pop();
    y.pop();
    let r = [];
    r['x'] = [];
    r['x'] = (x);
    r['y'] = y;
    //console.log("r:::")
    //console.log(r)
    return r;
  }

  public getTabela(user, pass, end): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('/api/epm' + end, (enviar), {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => {
        //console.log("JSON RESPOSTA::: "+res);
        return res.json();
      }
      );
  }


  public getValoresEpmRest(user, pass, end): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('/api/' + end, (enviar), {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => {
        //console.log("JSON RESPOSTA::: ");
        //console.log(res);
        return res.json();
      }
      );
  }

  ////// ----  GRAFICO LINHA

  showLegendLine = true;
  showXAxisLine = true;
  showYAxisLine = true;
  showXAxisLabelLine = false;
  xAxisLabelLine = 'Potência';
  showYAxisLabelLine = false;
  yAxisLabelLine = '';
  colorSchemeLine: any;
  themeSubscriptionLine: any;
  animations = false;
  timeline = true;
  autoScale = true;
  roundDomains = true;

  xAxisTickFormatting = (cell => moment(cell).format('D/M, H[h]'));
  // xAxisTickFormatting = (cell => moment(cell.value).format('LLLL') );


  public deleteMessage(id): any{
    //let objJson =  { idPrj: id };
    //alert('entrou no delete')
       let headers = new Headers({ 'Content-Type' : 'application/json' });
       return this.http.post(this.api+'candeias/deleteMessage', id, {  /// OLD ap/atividadesDia
        headers: headers,
        method:"POST"
      }).map(
        (res:Response) => {return res.json();}
      );
   }
   

  delete(msg){
    if(msg){
      this.deleteMessage(msg.id).subscribe(
             data=> {
                      //console.log('chegou json::: ' + JSON.stringify(data));
             },
             err=> console.log(err)
      );
    }
    this.getListaMensagens();
  }
  getListaMensagens(){
    this.getUserMessages(this.user).subscribe(
      data=> {
               this.listaMsg = data;
              //  this.storage.set("payload", this.listaMsg);
                console.log('chegou json::: ' + JSON.stringify(this.listaMsg));
            
      },
      err=> console.log(err)
);
  }
  listaMsg = [];
  api : string = '/app/r/';
  public getUserMessages(id): any{
    //let objJson =  { idPrj: id };
       let headers = new Headers({ 'Content-Type' : 'application/json' });
       return this.http.post(this.api+'candeias/userMessages', ''+id+'', {  /// OLD ap/atividadesDia
        headers: headers,
        method:"POST"
      }).map(
        (res:Response) => {return res.json();}
      );
   }

  formataDataInteira(data){
		//return moment(data).format('LLL')
    return moment(data).format('L') + ' ' + moment(data).format('LT');
	}

	diferencaTempo(data){
		let tempoFormatado = '';

		if(moment().diff(moment(data), 'minutes')>1){
			tempoFormatado = moment().diff(moment(data), 'minutes') + ' minutos atrás'
		}
    if(moment().diff(moment(data), 'hours')>0){
			tempoFormatado = moment().diff(moment(data), 'hours') + ' hora atrás'
		}
    if(moment().diff(moment(data), 'hours')>1){
			tempoFormatado = moment().diff(moment(data), 'hours') + ' horas atrás'
		}
		if(moment().diff(moment(data), 'days')>0){
			tempoFormatado = moment().diff(moment(data), 'days') + ' dia atrás'
		}
		if(moment().diff(moment(data), 'days')>1){
			tempoFormatado = moment().diff(moment(data), 'days') + ' dias atrás'
		}
		if(moment().diff(moment(data), 'years')>0){
			tempoFormatado = moment().diff(moment(data), 'years') + ' ano atrás'
		}
		if(moment().diff(moment(data), 'years')>1){
			tempoFormatado = moment().diff(moment(data), 'years') + ' anos atrás'
		}

		return tempoFormatado;
	}

  parar = false;
  ngOnDestroy(): void {
    // this.parar = true;
    // clearInterval(this.verificaConexao.bind(this));
    // clearInterval(this.updGraficosNovo.bind(this));

    // this.websocket.close();
    // console.log("disparou fechar ws");

  }
}

export class Message {
  title: string;
  message: string;
  timestamp: string;
  icon: string;
  color: string;
  constructor(_title: string, _message: string, _timestamp: string, _icon: string, _color: string) {
    this.title = _title; this.message = _message; this.timestamp = _timestamp; this.icon = _icon; this.color = _color;
  }
}
