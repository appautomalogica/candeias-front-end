import { StateService } from './../../@core/data/state.service';
import { Component } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Http, Response, Headers, ResponseContentType, RequestOptions } from '@angular/http';
//import * as echarts from 'echarts';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { forEach } from '@angular/router/src/utils/collection';
import { isNull } from 'util';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {
  optionsHistDisp: { backgroundColor: any; color: any[]; tooltip: { trigger: string; axisPointer: { type: string; label: { backgroundColor: any; }; }; }; grid: { top: string; left: string; right: string; bottom: string; containLabel: boolean; }; xAxis: { type: string; boundaryGap: boolean; data: any; axisTick: {}; axisLine: { lineStyle: { color: any; }; }; axisLabel: { show: boolean; interval: number; rotate: number; margin: number; formatter: string; textStyle: { color: any; fontFamily: string; fontSize: number; fontStyle: string; fontWeight: string; }; }; }[]; yAxis: { name: string; type: string; axisLine: { lineStyle: { color: any; }; }; splitLine: { lineStyle: { color: any; }; }; axisLabel: { textStyle: { color: any; }; }; }[]; series: { name: string; type: string; itemStyle: { normal: { color: string; lineStyle: { width: number; }; }; emphasis: { color: string; }; }; data: any; smooth: boolean; animationDelay: (idx: any) => number; }[]; };

  v: any = [];

  v11: number = 0;
  v12: number = 0;
  v21: number = 0;
  v22: number = 0;
  v31: number = 0;
  v32: number = 0;
  v41: number = 0;
  v42: number = 0;
  t101: number = 0;
  t102: number = 0;
  t103: number = 0;
  t104: number = 0;
  t121: number = 0;
  t122: number = 0;
  t131: number = 0;
  t132: number = 0;
  t180: number = 0;
  t200: number = 0;
  numeroTotalMotores: number;

  valor: any;
  inef: any;
  indisp: any;
  ociosa: any;

  valorG1: any;
  inefG1: any;
  indispG1: any;

  valorG2: any;
  inefG2: any;
  indispG2: any;

  types = ["12 horas", "1 dia", "1 semana", "15 dias"];
  type: any = "12 horas";

  status: string = "não iniciado";
  connected: boolean = false;
  //websocket: any;
  // url: string = 'ws://cec-termoeletrica.ddns.net:8765/';
  // url: string = 'ws://cec-termoeletrica.ddns.net:8181/Servidor';
  url: string = 'ws://cec-termoeletrica.ddns.net/ws/Servidor';
  websocket: WebSocket = new WebSocket(this.url);

  optionsG1: any = {};
  optionsG2: any = {};
  //themeSubscriptionG1: any;

  multiDisp = [
    {
      name: 'Total',
      series: [
        {
          name: 'v1',
          value: 0,
        }
      ],
    },
    {
      name: 'Totalx',
      series: [
        {
          name: 'v1',
          value: 0,
        }
      ],
    },
    {
      name: 'Global 1',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'Global 2',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
  ];
  multiLine = [
    {
      name: 'Total',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'Global 1',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'Global 2',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
  ];
  multiFatorGeral = [
    {
      name: 'Total',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'Global 1',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'Global 2',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
  ];
  multiFatorCluster = [
    {
      name: 'C1',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'C2',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'C3',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'C4',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'C5',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'C6',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'C7',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
    {
      name: 'C8',
      series: [
        {
          name: 'v1',
          value: 0,
        },
      ],
    },
  ];
  Legenda = "Legenda";
  constructor(
    private http: Http,
    private theme: NbThemeService,
  ) {
    // console.log("this.multiLine")
    // console.log(this.multiLine)
    //console.log('dash start')

    //let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //console.log(currentUser)

    this.verificaConexao();
    setInterval(this.verificaConexao.bind(this), 1000);

    // this.updGraficos();
    // setInterval(this.updGraficos.bind(this), 30000);

    this.updGraficosNovo();
    setInterval(this.updGraficosNovo.bind(this), 5*60000);

    this.updGraficosFator();
    setInterval(this.updGraficosFator.bind(this), 5*60000);


    //updater.setStatus('opening');
    this.status = "opening";

    // var url = 'ws://localhost:8765/';
    this.connected = false;

    this.updGraficosHistDisp();
    setInterval(this.updGraficosHistDisp.bind(this), 30000);


    /// GRAFICO LINHA
    this.themeSubscriptionLine = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorSchemeLine = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });

  }

  private verificaConexao() {
    // React to connection status periodically.
    if (this.connected || this.parar) return;

    // Reconnect.
    this.websocket = new WebSocket(this.url);
    if (this.websocket !== undefined) {
      // On state-change, display status and decide whether to reconnect.
      this.websocket.onopen = this.websocket.onclose = this.websocket.onerror = (event) => {
        var code = this.websocket.readyState;
        //let code = 0;
        var codes = {
          0: "opening",
          1: "open",
          2: "closing",
          3: "closed"
        };
        //updater.setStatus(codes[code]);
        this.status = codes[code];
        console.log("status: " + this.status)
        this.connected = (code == 0 || code == 1);
      };

      this.websocket.onmessage = (event) => {
        //updater.showMessage(JSON.parse(event.data).msg, true);
        let v = JSON.parse(event.data);
        // console.log("onMessage ws")
        //console.log(v_novo)
        //console.log(typeof v)
        //let v=this.array2dict(v_novo);
        // console.log(v)
        this.addToLastValues(v);
        v = this.v;
        //this.valor = this.formata2casas(v['Dados.teste.Value']);
        //this.inef = this.formata2casas(v['Dados.teste1.Value']);
        //this.indisp = this.formata2casas(v['Dados.teste2.Value']);
        //console.log(v);
        // console.log(event);
        // console.log(event.data);
        let tempValor = this.getValue(v['CAN.CTRL.PotAtTot']);
        let tempIndisp = this.getValue(v['CAN.CTRL.TotIndisp']);
        let tempInef = this.getValue(v['CAN.CTRL.MetaPot']);
        this.valor = this.formata2casas(tempValor);
        this.indisp = this.formata2casas(tempIndisp);
        this.inef = this.formata2casas(tempInef);
        this.ociosa = this.formata2casas(297.6-tempValor-tempIndisp-tempInef);

        this.valorG1 = this.formata2casas(this.getValue(v['CAN.GI.CTRL.PotAtTot']));
        this.indispG1 = this.formata2casas(this.getValue(v['CAN.GI.CTRL.TotIndisp']));
        this.inefG1 = this.formata2casas(this.getValue(v['CAN.GI.CTRL.MetaPot']));

        this.valorG2 = this.formata2casas(this.getValue(v['CAN.GII.CTRL.PotAtTot']));
        this.indispG2 = this.formata2casas(this.getValue(v['CAN.GII.CTRL.TotIndisp']));
        this.inefG2 = this.formata2casas(this.getValue(v['CAN.GI.CTRL.MetaPot']));

        let Prod: number = (v['CAN_objMotores.cdoContadorCAN.Prod']);
        let Indisp: number = (v['CAN_objMotores.cdoContadorCAN.Indisp']);
        let Rest: number = (v['CAN_objMotores.cdoContadorCAN.Rest']);
        let CargaAnorm: number = (v['CAN_objMotores.cdoContadorCAN.CargaAnorm']);
        let Total: number = (v['CAN_objMotores.cdoContadorCAN.Total']);

        this.t101 = Math.round(v['CAN.UTIL.HFO.[T-101].NvlComb']);
        this.t102 = Math.round(v['CAN.UTIL.HFO.[T-102].NvlComb']);
        this.t103 = Math.round(v['CAN.UTIL.HFO.[T-103].NvlComb']);
        this.t104 = Math.round(v['CAN.UTIL.HFO.[T-104].NvlComb']);
        this.t121 = Math.round(v['CAN.UTIL.HFO.[T-121].NvlComb']);
        this.t122 = Math.round(v['CAN.UTIL.HFO.[T-122].NvlComb']);
        this.t131 = Math.round(v['CAN.UTIL.HFO.[T-131].NvlComb']);
        this.t132 = Math.round(v['CAN.UTIL.HFO.[T-132].NvlComb']);

        this.t180 = Math.round(v['CAN.UTIL.DO.[T-180].NvlComb']);
        this.t200 = Math.round(v['CAN.UTIL.LUB.[T-200].NvlOl']);

        //console.log("CargaAnorm:: "+CargaAnorm)
        this.numeroTotalMotores = Total;
        this.v11 = Total - Indisp;
        this.v12 = Indisp;
        this.v21 = Prod;
        this.v22 = Total - Indisp - Prod;
        this.v31 = Prod - Rest;
        this.v32 = Rest;
        this.v41 = Prod - CargaAnorm;
        this.v42 = CargaAnorm;

        //console.log(this.valor + " | " + this.inef + " | " + this.indisp)
        // console.log( Total + " | " + Prod + " | " + Indisp + " | " + Rest  + " | " + CargaAnorm )
      };

      // State is now "opening", although it may fail in the future.
      this.connected = true;
    }

  }

  addToLastValues(obj: any): any {

    let v = this.v;
    Object.keys(obj).forEach(function (key) {

      //console.log(key, obj[key]);
      v[key] = obj[key].v;
    });
    this.v = v;
  }

  private array2dict(a) {
    let d = [];
    for (var i = 0, len = a.length; i < len; ++i) {
      let p = a[i].p;
      d[p] = a[i].v;
    }
    return d;
  }

  private updGraficosHistDisp() {

    this.getValoresEpmRest('user1', 'user1_secret', 'epmTotalGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Total', data);
      },
      err => console.log(err)
    );

    this.getValoresEpmRest('user1', 'user1_secret', 'epmProdGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Produzindo', data);
      },
      err => console.log(err)
    );

    this.getValoresEpmRest('user1', 'user1_secret', 'epmRestGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Com restrição', data);
      },
      err => console.log(err)
    );

    this.getValoresEpmRest('user1', 'user1_secret', 'epmIndispGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Indisponíveis', data);
      },
      err => console.log(err)
    );


    this.getValoresEpmRest('user1', 'user1_secret', 'epmOciososGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Ociosos', data);
      },
      err => console.log(err)
    );

  }
  xHist;
  y1;
  y2;
  y3;
  y4;

  private updGraficosFator() {

    // tabela de fatores
    let TOTAL = 297.6;

    let G1 = 148.8;
    let G2 = 148.8;

    let GG01 = 34.72;
    let GG02 = 44.64;
    let GG03 = 34.72;
    let GG04 = 34.72;
    let GG05 = 34.72;
    let GG06 = 34.72;
    let GG07 = 34.72;
    let GG08 = 44.64;

    let porMotorNormal = 2.17;
    let porMotorMaior = 9.3;

    this.getTabelaFator(TOTAL, 'SE_BGI_14W1_Med_PotGI,SE_BGII_14W2_Med_PotGII').subscribe(
      data => {
        this.processaValoresFator('Total', data);
        //console.log("fator")
        //console.log(data)
      },
      err => console.log(err)
    );

    this.getTabelaFator(G1, 'SE_BGI_14W1_Med_PotGI').subscribe(
      data => {
        this.processaValoresFator('Global 1', data);
        //console.log("fator")
        //console.log(data)
      },
      err => console.log(err)
    );

    this.getTabelaFator(G2, 'SE_BGII_14W2_Med_PotGII').subscribe(
      data => {
        this.processaValoresFator('Global 2', data);
        //console.log("fator")
        //console.log(data)
      },
      err => console.log(err)
    );

    // this.getTabelaFator(GG01, 'CAN_GI_CTRL_CL1_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 1', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );

    // this.getTabelaFator(GG02, 'CAN_GI_CTRL_CL2_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 2', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );

    // this.getTabelaFator(GG03, 'CAN_GI_CTRL_CL3_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 3', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );

    // this.getTabelaFator(GG04, 'CAN_GI_CTRL_CL4_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 4', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );


    // this.getTabelaFator(GG05, 'CAN_GII_CTRL_CL5_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 5', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );

    // this.getTabelaFator(GG06, 'CAN_GII_CTRL_CL6_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 6', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );

    // this.getTabelaFator(GG07, 'CAN_GII_CTRL_CL7_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 7', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );

    // this.getTabelaFator(GG08, 'CAN_GII_CTRL_CL8_PotAtTot').subscribe(
    //   data => {
    //     this.processaValoresFator('Cluster 8', data);
    //     console.log("fator")
    //     console.log(data)
    //   },
    //   err => console.log(err)
    // );




  }

  private updGraficosNovo() {


    this.getTabela('user1', 'user1_secret', 'Motor1').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Global 1', data);
      },
      err => console.log(err)
    );

    this.getTabela('user1', 'user1_secret', 'Motor2').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Global 2', data);
      },
      err => console.log(err)
    );

    this.getTabela('user1', 'user1_secret', 'PotIndispGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Indisponível', data);
      },
      err => console.log(err)
    );

    this.getTabela('user1', 'user1_secret', 'PotInefGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Ineficiência', data);
      },
      err => console.log(err)
    );

    this.getTabela('user1', 'user1_secret', 'MotorTotal').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Gerada', data);
      },
      err => console.log(err)
    );

    this.getTabela('user1', 'user1_secret', 'PotOciosaGeral').subscribe(
      data => {
        this.processaValoresRecebidosSQLparaGraficosNovo('Ociosa', data);
      },
      err => console.log(err)
    );

  }

  private getValue(v) {

    if (isNaN(v) || isNull(v)) {
      return null;
    }
    if (v !== undefined) {
      if (v.length == 3) {
        if (v[1] > 127) {
          return v[2];
        }
      } else {
        return v;
      }
    }
    return null;
  }

  public formata2casas(v) {
    if (isNaN(v) || isNull(v)) {
      return 0;
    }
    v = v + 0;
    v = Math.round(v * 100) / 100;
    return v;
  }

  public processaValoresRecebidosSQLparaGraficos(v: any): any {
    let x = [];
    let y = [];
    for (var i = 0, len = v.length; i < len; ++i) {
      //console.log(v[i].pot + " | "+ moment(v[i].ts).format('LLL'))
      x.push(moment(v[i].ts).format('D/M, H') + 'h');
      if (v[i].pot == null) {
        y.push(0);
      } else {
        y.push(Math.round(v[i].pot));
      }
    }
    // x.pop();
    // y.pop();
    let r = [];
    r['x'] = [];
    r['x'] = (x);
    r['y'] = y;
    //console.log("r:::")
    //console.log(r)
    return r;
  }

  public processaValoresFator(nomepena, v: any): any {
    const ret: { name, series } = { name: nomepena, series: [] };

    for (var i = 0, len = v.length; i < len; ++i) {
      const d: { name, value } = { name: '', value: '' };
      d.name = (moment(v[i].ts).toDate());
      // d.value = Math.round(v[i].valor);
      d.value = (v[i].valor);
      if (v[i].valor == null) {
        d.value = 0;
      } else {
        // d.value = Math.round(v[i].valor);
        d.value = (v[i].valor);
      }
      ret.series.push(d);
    }

    if (nomepena === 'Total') {
      this.multiFatorGeral[0] = ret;
    }
    if (nomepena === 'Global 1') {
      this.multiFatorGeral[1] = ret;
    }
    if (nomepena === 'Global 2') {
      this.multiFatorGeral[2] = ret;
    }
    this.multiFatorGeral = [...this.multiFatorGeral];

    if (nomepena === 'Cluster 1') {
      this.multiFatorCluster[0] = ret;
    }
    if (nomepena === 'Cluster 1') {
      this.multiFatorCluster[0] = ret;
    }
    if (nomepena === 'Cluster 2') {
      this.multiFatorCluster[1] = ret;
    }
    if (nomepena === 'Cluster 3') {
      this.multiFatorCluster[2] = ret;
    }
    if (nomepena === 'Cluster 4') {
      this.multiFatorCluster[3] = ret;
    }
    if (nomepena === 'Cluster 5') {
      this.multiFatorCluster[4] = ret;
    }
    if (nomepena === 'Cluster 6') {
      this.multiFatorCluster[5] = ret;
    }
    if (nomepena === 'Cluster 7') {
      this.multiFatorCluster[6] = ret;
    }
    if (nomepena === 'Cluster 8') {
      this.multiFatorCluster[7] = ret;
    }
    this.multiFatorCluster = [...this.multiFatorCluster];



    this.themeSubscriptionLine = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorSchemeLine = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
    //return ret;
  }
  // multiLine:any;


  public processaValoresRecebidosSQLparaGraficosNovo(nomepena, v: any): any {
    // let ret: pena = new pena();
    // ret.name = nomepena;
    // ret.series = [];
    const ret: { name, series } = { name: nomepena, series: [] };

    for (var i = 0, len = v.length; i < len; ++i) {
      const d: { name, value } = { name: '', value: '' };
      // d.name = (moment(v[i].ts).format('D/M, H') + 'h');
      d.name = (moment(v[i].ts).toDate());
      d.value = Math.round(v[i].valor);
      if (v[i].valor == null) {
        d.value = 0;
      } else {
        d.value = Math.round(v[i].valor);
      }
      ret.series.push(d);
    }

    // console.log(ret)
    // if(nomepena==='Total'){
    //   this.multiLine[0]=ret;
    // }
    // if(nomepena==='Global 1'){
    //   this.multiLine[1]=ret;
    // }
    // if(nomepena==='Global 2'){
    //   this.multiLine[2]=ret;
    // }

    if (nomepena === 'Gerada') {
      this.multiLine[0] = ret;
    }
    if (nomepena === 'Indisponível') {
      this.multiLine[1] = ret;
    }
    if (nomepena === 'Ineficiência') {
      this.multiLine[2] = ret;
    }
    if (nomepena === 'Ociosa') {
      this.multiLine[3] = ret;
    }
    this.multiLine = [...this.multiLine];

    if (nomepena === 'Total') {
      this.multiDisp[0] = ret;
      this.multiDisp = [...this.multiDisp];
    }

    if (nomepena === 'Produzindo') {
      this.multiDisp[1] = ret;
      this.multiDisp = [...this.multiDisp];
    }

    if (nomepena === 'Com restrição') {
      this.multiDisp[2] = ret;
      this.multiDisp = [...this.multiDisp];
    }

    if (nomepena === 'Indisponíveis') {
      this.multiDisp[3] = ret;
      this.multiDisp = [...this.multiDisp];
    }

    if (nomepena === 'Ociosos') {
      this.multiDisp[4] = ret;
      this.multiDisp = [...this.multiDisp];
    }
    //  console.log(nomepena)
    //  console.log(this.multiLine)
    //  console.log(this.multiDisp)

    this.themeSubscriptionLine = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorSchemeLine = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
    //return ret;
  }
  // multiLine:any;

  public processaValoresRecebidosSQLparaGraficosHist(v: any): any {
    let x = [];
    let y = [];
    for (var i = 0, len = v.length; i < len; ++i) {
      //console.log(v[i].pot + " | "+ moment(v[i].ts).format('LLL'))
      x.push(moment(v[i].ts).format('D/M, H') + 'h');
      if (v[i].valor == null) {
        y.push(0);
      } else {
        y.push(Math.round(v[i].valor));
      }
    }
    x.pop();
    y.pop();
    let r = [];
    r['x'] = [];
    r['x'] = (x);
    r['y'] = y;
    //console.log("r:::")
    //console.log(r)
    return r;
  }

  public getTabela(user, pass, end): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('/api/epm' + end, (enviar), {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => {
        //console.log("JSON RESPOSTA::: "+res);
        return res.json();
      }
      );
  }



  getLogin(user, pass): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    // return this.http.post('http://cec-termoeletrica.ddns.net:81/auth', (enviar), {
    return this.http.post('/api/auth', (enviar), {
      headers: headers,
      method: "POST"
    })
    // .map(
    //   (res: Response) => {
    //     //console.log("JSON RESPOSTA::: "+res);
    //     return res.json();
    //   }
    //   );
  }

  public getTabelaFator(fator, pathname): any {
    let enviar = { 'fator': fator, 'pathname': pathname };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('/api/epmFator', (enviar), {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => {
        //console.log("JSON RESPOSTA::: "+res);
        return res.json();
      }
      );
  }

  public getValoresEpmRest(user, pass, end): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('/api/' + end, (enviar), {
      headers: headers,
      method: "POST"
    }).map(
      (res: Response) => {
        //console.log("JSON RESPOSTA::: ");
        //console.log(res);
        return res.json();
      }
      );
  }

  ////// ----  GRAFICO LINHA
  getTTData(i){
    return moment(i.name).format('D/M, H[h]')
  }

  getTTValor(i){
    return  i.value
  }

  getTTNome(i){
    return  i.series
    }

  viewPot: any[] = [null, null];
  showLegendLine = true;
  showXAxisLine = true;
  showYAxisLine = true;
  showXAxisLabelLine = false;
  xAxisLabelLine = 'Potência';
  showYAxisLabelLine = false;
  yAxisLabelLine = '';
  colorSchemeLine: any;
  themeSubscriptionLine: any;
  animations = false;
  timeline = true;
  autoScale = true;
  roundDomains = true;

  showRefLines=true;
  referenceLines = [{name:'', value:0.85}];
  xAxisTickFormatting = (cell => moment(cell).format('D/M, H[h]'));
  // xAxisTickFormatting = (cell => moment(cell.value).format('LLLL') );

  parar = false;
  ngOnDestroy(): void {
    this.parar = true;
    clearInterval(this.verificaConexao.bind(this));
    clearInterval(this.updGraficosNovo.bind(this));
    clearInterval(this.updGraficosFator.bind(this));
    this.websocket.close();
    console.log("disparou fechar ws");

  }
}
