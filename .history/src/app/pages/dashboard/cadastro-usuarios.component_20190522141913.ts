import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import * as CryptoJS from 'crypto-js';
import { NbThemeService, NbColorHelper } from '@nebular/theme';


@Component({
  selector: 'app-cadastro-usuarios',
  templateUrl: './cadastro-usuarios.component.html',
  styleUrls: ['./cadastro-usuarios.component.scss'],
  styles: [`
    [nbButton] {
      margin-right: 1rem;
      margin-bottom: 1rem;
    }
  `],
})
export class CadastroUsuariosComponent implements OnInit {

  constructor(private http: Http) { }

  username: string;
  password: string;
  api: string = 'http://cec-termoeletrica.ddns.net/app/r/';
  postData:  string;

  ngOnInit() {
  }

  postCadastroUsuario(user, pass): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    // return this.http.post('http://cec-termoeletrica.ddns.net:81/auth', (enviar), {
    return this.http.post('http://localhost:81/cadastrousuario', (enviar), {
      headers: headers,
      method: "POST"
    })
  }

  formSubmit() {
    if (!this.username) {
      alert('insira o usuário')
    } else if(!this.password) {
      alert('insira a senha')
    } else {
      this.postCadastroUsuario(this.username, CryptoJS.MD5(this.password)+'').subscribe(
        (data: any) => {
          if(data==true) {
            console.log("ENTREIIIIII")
          }
        }
        /* data => this.postData = JSON.stringify(data),
        error => alert(error) */
      );
      console.log(this.postData);
    }
  }

}
