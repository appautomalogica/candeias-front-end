import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import * as CryptoJS from 'crypto-js';
import { NbThemeService, NbColorHelper } from '@nebular/theme';


@Component({
  selector: 'app-cadastro-usuarios',
  templateUrl: './cadastro-usuarios.component.html',
  styleUrls: ['./cadastro-usuarios.component.scss'],
  styles: [`
    [nbButton] {
      margin-right: 1rem;
      margin-bottom: 1rem;
    }
  `],
})
export class CadastroUsuariosComponent implements OnInit {

  constructor(private http: Http) { }

  username: string;
  password: string;
  api: string = 'http://cec-termoeletrica.ddns.net/app/r/';
  postData:  string;
  public strOk = ''

  ngOnInit() {
  }

  postCadastroUsuario(user, pass): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    // return this.http.post('http://cec-termoeletrica.ddns.net:81/auth', (enviar), {
    return this.http.post('http://localhost:81/cadastrousuario', (enviar), {
      headers: headers,
      method: "POST"
    })
  }

  setMyStyles(status) {
    let styles = {
      'color': !status ? '#00FF21' : '#00FF21',
    };
    return styles;
  }

  formSubmit() {
    if (!this.username) {
      this.setMyStyles(false);
      this.strOk = 'Insira o nome do usuário';
    } else if(!this.password) {
      this.setMyStyles(false);
      this.strOk = 'Insira a senha';
    } else {
      this.postCadastroUsuario(this.username, CryptoJS.MD5(this.password)+'').subscribe(
        (data: Response) => {
          if (data) {
            if (data.status === 201) {
              this.setMyStyles(false);
              this.strOk = 'Algo de errado aconteceu.';
            } else if (data.status === 200) {
              this.setMyStyles(true);
              this.strOk = 'Usuário adicionado com sucesso.';
            }
          }
        }
      );
      console.log(this.postData);
    }
  }

}
