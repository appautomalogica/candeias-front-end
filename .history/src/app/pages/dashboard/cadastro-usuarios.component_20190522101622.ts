import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { sha256 } from 'js-sha256';

@Component({
  selector: 'app-cadastro-usuarios',
  templateUrl: './cadastro-usuarios.component.html',
  styleUrls: ['./cadastro-usuarios.component.scss']
})
export class CadastroUsuariosComponent implements OnInit {

  constructor(private _http: Http) { }

  username: string;
  password: string;
  api : string = 'http://cec-termoeletrica.ddns.net/app/r/';

  ngOnInit() {
  }

  /* public getTabelaPotencia(username, password): any {
    let enviar = {'usuario': username, 'password': sha256(password) };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this._http.post(this.api + 'candeias/cadastrousuarios', (enviar), {
      headers: headers,
      method: "POST"
    }).pipe(map(res => res.json()));
  } */

  formSubmit() {
    if (!this.username) {
      alert('insira o usuário')
    } else if(!this.password) {
      alert('insira a senha')
    } else {
      let currentUser = JSON.parse(localStorage.getItem('candeias'));

      let enviar = {'usuario': this.username, 'password': sha256(this.password) };
      let headers = new Headers({ 'Content-Type': 'application/json' });
      return this._http.post(this.api + 'candeias/cadastrousuarios', (enviar), {
        headers: headers,
        method: "POST"
      }).pipe(map(res => res.json()));

    }
  }

}
