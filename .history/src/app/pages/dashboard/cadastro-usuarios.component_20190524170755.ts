import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import * as CryptoJS from 'crypto-js';
import { NbThemeService, NbColorHelper } from '@nebular/theme';


@Component({
  selector: 'ngx-app-cadastro-usuarios',
  templateUrl: './cadastro-usuarios.component.html',
  styleUrls: ['./cadastro-usuarios.component.scss'],
  styles: [`
    [nbButton] {
      margin-right: 1rem;
      margin-bottom: 1rem;
    }
  `],
})
export class CadastroUsuariosComponent implements OnInit {

  constructor(private http: Http) { }

  public strOk = '';
  username: string;
  password: string;
  api = 'http://cec-termoeletrica.ddns.net:81/';
  postData:  string;
  status: boolean = false;

  ngOnInit() {
  }

  postCadastroUsuario(user, pass): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post('http://cec-termoeletrica.ddns.net/api/cadastrousuario', (enviar), {
    // return this.http.post('http://localhost:81/cadastrousuario', (enviar), {
      headers: headers,
      method: "POST"
    });
  }

  setMyStyles() {
    let styles = {
      'color': !this.status ? 'red' : '#00BF21',
    };
    return styles;
  }

  formSubmit() {
    if (!this.username) {
      this.status = false;
      this.setMyStyles();
      this.strOk = 'Insira o nome do usuário';
    } else if(!this.password) {
      this.status = false;
      this.setMyStyles();
      this.strOk = 'Insira a senha';
    } else {
      this.postCadastroUsuario(this.username, CryptoJS.MD5(this.password) + '').subscribe(
        (data: Response) => {
          if (data) {
            if (data.status === 201) {
              this.status = false;
              this.setMyStyles();
              this.strOk = 'Algo de errado aconteceu.';
            } else if (data.status === 200) {
              this.status = true;
              this.setMyStyles();
              this.strOk = 'Usuário cadastrado com sucesso.';
              this.username = '';
              this.password = '';
            }
          }
        },
      );
    }
  }

}
