import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { sha256 } from 'js-sha256';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-cadastro-usuarios',
  templateUrl: './cadastro-usuarios.component.html',
  styleUrls: ['./cadastro-usuarios.component.scss']
})
export class CadastroUsuariosComponent implements OnInit {

  constructor(private http: Http,) { }

  username: string;
  password: string;
  api : string = 'http://cec-termoeletrica.ddns.net/app/r/';
  postData :  string;

  ngOnInit() {
  }

  getLogin(user, pass): any {
    let enviar = { username: user, password: pass };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    // return this.http.post('http://cec-termoeletrica.ddns.net:81/auth', (enviar), {
    return this.http.post('http://localhost:81/cadastrousuario', (enviar), {
      headers: headers,
      method: "POST"
    })
    // .map(
    //   (res: Response) => {
    //     //console.log("JSON RESPOSTA::: "+res);
    //     return res.json();
    //   }
    //   );
  }

  formSubmit() {
    if (!this.username) {
      alert('insira o usuário')
    } else if(!this.password) {
      alert('insira a senha')
    } else {
      this.getLogin(this.username, CryptoJS.MD5(this.password)+'').subscribe(
        data => this.postData = JSON.stringify(data),
        error => alert(error)
      );
    }
  }

}
