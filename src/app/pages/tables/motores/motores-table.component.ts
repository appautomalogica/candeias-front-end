import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Http, Response, Headers, ResponseContentType, RequestOptions } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import * as moment from 'moment';
import 'moment/locale/pt-br';

import * as FileSaver from 'file-saver';
import { webSocket } from 'rxjs/observable/dom/webSocket';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalMotoresComponent } from '../../ui-features/modals/modal/modalMotores.component';
import { MotorRealTimeService } from '../../ui-features/modals/modal/modalMotores.service';
import { isNull } from 'util';

@Component({
  selector: 'ngx-motores-table',
  templateUrl: './motores-table.component.html',
  // styleUrls: ['./motores-table.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class MotoresTableComponent {

  v: any = [];
  lista:linha[];

  status: string = "não iniciado";
  connected: boolean = false;
  //websocket: any;
  // url: string = 'ws://cec-termoeletrica.ddns.net:8765/';
  // url: string = 'ws://cec-termoeletrica.ddns.net:8181/Motores';
  url: string = 'ws://cec-termoeletrica.ddns.net/ws/Motores';
  websocket: WebSocket = new WebSocket(this.url);
  
  settings = {
    pager: {
      perPage : 10
    },
    noDataMessage: "",
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      grupo: {
        title: 'Grupo',
        type: 'text',
        class: 'teste',
        width: '3%',
        filter: {
          type: 'list',
          config: {
            selectText: '',
            list: [
              { value: 'GI ', title: 'GI ' },
              { value: 'GII ', title: 'GII ' },
            ],
          },
        },
      },
      nome: {
        title: 'Motor',
        type: 'text',
        class: '',
        width: '25%',
        // filter : {
        //   type: 'completer',
        //   config: {
        //     completer: {
        //       data: this.lista,
        //       searchFields: 'nome',
        //       titleField: 'nome',
        //     },
        //   },
        // }
      },
      // Indisp: {
      //   title: 'Indisponível',
      //   type: 'number',
      //   class: '',
      //   width: '10%',
      // },
      // manut: {
      //   title: 'Manutenção',
      //   type: 'number',
      //   class: '',
      //   width: '10%',
      // },
      // gerando: {
      //   title: 'Gerando',
      //   type: 'number',
      //   class: '',
      //   width: '10%',
      // },
      horimetro: {
        title: 'Horímetro (h)',
        type: 'number',
        class: '',
        width: '20%',
        filter: false,
          
        compareFunction(dir?: any, value1?: any, value2?: any) {
          value1 = value1 + 0;
          if(isNull(value1) || isNaN(value1) || value1==="undefined"){
            value1 = 0;
          }
          if(isNull(value2) || isNaN(value2) || value2==="undefined"){
            value2 = 0;
          }
          if (dir == -1)
            return value2 - value1;
            // return moment(value2,formato).diff( moment(value1,formato), 'seconds') ;
          else
          return value1 - value2;
            // return moment(value1,formato).diff( moment(value2,formato), 'seconds') ;
          },
        
      },
      Estado: {
        title: 'Estado',
        type: 'html',
        class: '',
        width: '30%',
        filter: {
          type: 'list',
          config: {
            selectText: '',
            list: [
              { value: 'Disponível ', title: 'Disponível' },
              { value: 'Gerando', title: 'Gerando' },
              { value: 'Indisponível', title: 'Indisponível' },
              { value: 'Sem comunicação', title: 'Sem comunicação' },
              { value: 'Manutenção', title: 'Manutenção' },
              { value: 'Com Restrição', title: 'Com Restrição' },
              
            ],
          },
        },
      },
      potencia: {
        title: 'Potência (kW)',
        type: 'number',
        class: '',
        width: '20%',
        filter: false,

          
        compareFunction(dir?: any, value1?: any, value2?: any) {
          value1 = value1 + 0;
          if(isNull(value1) || isNaN(value1) || value1==="undefined"){
            value1 = 0;
          }
          if(isNull(value2) || isNaN(value2) || value2==="undefined"){
            value2 = 0;
          }
          if (dir == -1)
            return value2 - value1;
            // return moment(value2,formato).diff( moment(value1,formato), 'seconds') ;
          else
          return value1 - value2;
            // return moment(value1,formato).diff( moment(value2,formato), 'seconds') ;
          },
      },
      // data: {
      //   title: 'Data',
      //   type: 'Date',
      //   compareFunction(dir?: any, value1?: any, value2?: any) {
      //     let formato = "LLL";
      //     //console.log(dir + " | " + moment(value1,formato) + " ---- " + moment(value2,formato) + " = " + moment(value1,formato).diff( moment(value2,formato), 'seconds')  );
      //     if (dir == -1)
      //       return moment(value2, formato).diff(moment(value1, formato), 'seconds');
      //     else
      //       return moment(value1, formato).diff(moment(value2, formato), 'seconds');
      //   },

      // },
    },
  };

  settingsMobile = {
    pager: {
      perPage : 10
    },
    noDataMessage: "",
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      grupoM: {
        title: 'G',
        type: 'html',
        class: 'teste',
        width: '10',
        filter: false,
      },
      nomeM: {
        title: 'M',
        type: 'html',
        class: '',
        width: '10',
        filter: false,
        // filter : {
        //   type: 'completer',
        //   config: {
        //     completer: {
        //       data: this.lista,
        //       searchFields: 'nome',
        //       titleField: 'nome',
        //     },
        //   },
        // }
      },
      // Indisp: {
      //   title: 'Indisponível',
      //   type: 'number',
      //   class: '',
      //   width: '10%',
      // },
      // manut: {
      //   title: 'Manutenção',
      //   type: 'number',
      //   class: '',
      //   width: '10%',
      // },
      // gerando: {
      //   title: 'Gerando',
      //   type: 'number',
      //   class: '',
      //   width: '10%',
      // },
      iconeEstado: {
        title: 'E',
        type: 'html',
        class: '',
        width: '10',
        filter: false,
      },

      potenciaM: {
        title: 'P (kW)',
        type: 'html',
        class: '',
        width: '10',
        filter: false,
      },
      // data: {
      //   title: 'Data',
      //   type: 'Date',
      //   compareFunction(dir?: any, value1?: any, value2?: any) {
      //     let formato = "LLL";
      //     //console.log(dir + " | " + moment(value1,formato) + " ---- " + moment(value2,formato) + " = " + moment(value1,formato).diff( moment(value2,formato), 'seconds')  );
      //     if (dir == -1)
      //       return moment(value2, formato).diff(moment(value1, formato), 'seconds');
      //     else
      //       return moment(value1, formato).diff(moment(value2, formato), 'seconds');
      //   },

      // },
    },
  };
  pnSelected;
  conta()
    {
      if(this.pnSelected !== undefined){
        if(this.prelista[this.pnSelected] !== undefined){
          if(this.prelista[this.pnSelected].potencia !== undefined){
            this.servMotor.v = this.prelista[this.pnSelected].potencia;
            // console.log( "this.servMotor.v")  
            // console.log( this.servMotor.v)
          }
          if(this.prelista[this.pnSelected].Estado !== undefined){
            let est = this.prelista[this.pnSelected].Estado.split(">")[1];
            this.servMotor.estado = est;
            // console.log( "this.servMotor.estado")  
            // console.log( this.servMotor.estado)
          }

          if(this.prelista[this.pnSelected].nome !== undefined){
            this.servMotor.nome = this.prelista[this.pnSelected].nome;
            // console.log( "this.servMotor.nome")  
            // console.log( this.servMotor.nome)
          }

           if(this.prelista[this.pnSelected].grupo !== undefined){
            this.servMotor.grupo = this.prelista[this.pnSelected].grupo;
            // console.log("this.prelista[this.pnSelected].grupo")
            // console.log(this.prelista[this.pnSelected].grupo)
          }

          if(this.prelista[this.pnSelected].id !== undefined){
            this.servMotor.id = this.prelista[this.pnSelected].id;
            // console.log("this.prelista[this.pnSelected].id")
            // console.log(this.prelista[this.pnSelected].id)
          }

          if(this.prelista[this.pnSelected].horimetro !== undefined){
            this.servMotor.horimetro = this.prelista[this.pnSelected].horimetro;
            // console.log("this.prelista[this.pnSelected].horimetro")
            // console.log(this.prelista[this.pnSelected].horimetro)
          }
          this.servMotor.isMobile = this.isMobile;
        }
      }
      // console.log("prelista");
      // console.log(this.prelista);
    }
  source: LocalDataSource = new LocalDataSource();
  constructor(
    private http: Http,
    private modalService: NgbModal,
    private servMotor : MotorRealTimeService,
  ) {
    this.calcWidth();
    console.log("É mobile? "+this.isMobile)
    this.verificaConexao();
    setInterval(this.verificaConexao.bind(this), 1000);


    setInterval(this.conta.bind(this), 1000);


    this.source.setSort([{
      field: 'id', direction: 'asc',
    }]);

  }
  private verificaConexao() {
    // React to connection status periodically.
    if (this.connected || this.parar) return;

    // Reconnect.
    this.websocket = new WebSocket(this.url);
    if (this.websocket !== undefined) {
      // On state-change, display status and decide whether to reconnect.
      this.websocket.onopen = this.websocket.onclose = this.websocket.onerror = (event) => {
        var code = this.websocket.readyState;
        //let code = 0;
        var codes = {
          0: "opening",
          1: "open",
          2: "closing",
          3: "closed"
        };
        //updater.setStatus(codes[code]);
        this.status = codes[code];
        console.log("status MOT: " + this.status)
        this.connected = (code == 0 || code == 1);
      };

      this.websocket.onmessage = (event) => {
        
        let v = JSON.parse(event.data);
        
        this.addToLastValues(v);
        v = this.v;
        this.montaLista();
       
        //  console.log("raw");
        //  console.log(v);
        //  console.log(this.prelista);
        };

     this.connected = true;
    }

  }

  addToLastValues(obj: any): any {

    let v = this.v;
    Object.keys(obj).forEach(function (key) {

      //console.log(key, obj[key]);
      v[key] = obj[key].v;
    });
    this.v = v;
  }
  
  prelista=[];
  montaLista() {
    // data=
    // ["REventos_2017119_1400.pdf", "REventos_2017119_1401.pdf", "REventos_2017119_1402.pdf", "REventos_2017119_1403.pdf", "REventos_2017119_1404.pdf", "REventos_2017119_1405.pdf"]
    let keys = Object.keys(this.v);

    for (var i = 0; i < keys.length; i++) {
      let pn = keys[i];
      let nome = pn.split(".");
      let grupo = nome[1];
      let tipo = nome[4];

      let id = nome[0]+"."+nome[1]+"."+nome[2];

      //let obj : { nome: string, grupo: string, potencia:number, Indisp:boolean, manut:boolean, gerando:boolean };
      let obj;
      if(this.prelista[id] === undefined){ 
        obj = new linha();
      }else{ 
        obj = this.prelista[id];
      }

      //console.log("obj")
      //console.log(obj)
      obj.nome= nome[2].replace("M","Motor ");
      obj.nomeM = "<font size=1>"+obj.nome+"</font>"
      obj.grupo=grupo+" ";
      obj.grupoM = "<font size=1>"+obj.grupo+"</font>"
      obj.id = id;

      if(tipo==="PotAt"){
        obj.potencia = this.v[pn];
        let p = obj.potencia;
        if(isNull(obj.potencia)){
          p = 0;
        }
        obj.potenciaM = "<font size=1>"+p+"</font>"
      }

      if(tipo==="Hor"){
        obj.horimetro = this.v[pn];
        if(isNull(obj.horimetro) || obj.horimetro==="" || isNaN(obj.horimetro) || obj.horimetro==="undefined"){
          obj.horimetro = 0;
        }
      }

      if(tipo==="Indisp"){
        obj.Indisp = this.v[pn];
        // if (this.v[pn] === 1){ 
        //   obj.Indisp = "Sim"
        // }else{ 
        //   obj.Indisp = "Não"
        // }
      }

      if(tipo==="Manut"){
        obj.manut = this.v[pn];
        // if (this.v[pn] === 1){ 
        //   obj.manut = "Sim"
        // }else{ 
        //   obj.manut = "Não"
        // }
      }

      if(tipo==="MSinc"){
        obj.gerando = this.v[pn];
        // if (this.v[pn] === 1){ 
        //   obj.gerando = "Sim"
        // }else{ 
        //   obj.gerando = "Não"
        // }
      }

      if(tipo==="MotRst"){
        obj.comRestricao = this.v[pn];
        // if (this.v[pn] === 1){ 
        //   obj.gerando = "Sim"
        // }else{ 
        //   obj.gerando = "Não"
        // }
      }

      let fontSize=4;
      if(this.calcWidth()<600){
        fontSize=2;
      }

      if(obj.gerando){
      // obj.Estado = "<font size="+fontSize+"px>Gerando</font>"
      obj.Estado = "<img class='imgTop' height='20' src='assets/images/gerando.svg'> Gerando"
      obj.iconeEstado = "<img class='imgTop' height='20' src='assets/images/gerando.svg'><br><font size=1>Gerando</font>"
      }
      else if (obj.manut){
        // obj.Estado = "<font size="+fontSize+"px>Manutenção</font>"
        obj.Estado = "<img class='imgTop' height='20' src='assets/images/manut.svg'> Manutenção"
        obj.iconeEstado = "<img class='imgTop' height='20' src='assets/images/manut.svg'><br><font size=1>Manut.</font>"
      }else if (obj.Indisp){
      // obj.Estado = "<font size="+fontSize+"px>Indisponível</font>"
      obj.Estado = "<img class='imgTop' height='20' src='assets/images/indisp.svg'> Indisponível"      
      obj.iconeEstado = "<img class='imgTop' height='20' src='assets/images/indisp.svg'><br><font size=1>Indisp.</font>"
    }else if ( obj.potencia === null ){
        // obj.Estado = "<font size="+fontSize+"px>Sem comunicação</font>";
        obj.Estado = "<img class='imgTop' height='20' src='assets/images/semcomun.svg'> Sem comunicação";
        obj.iconeEstado = "<img class='imgTop' height='20' src='assets/images/semcomun.svg'><br><font size=1>Sem<br>comun.</font>"
        obj.Indisp = null;
        obj.manut = null;
      }
      else {
        // if(this.isMobile){
          obj.iconeEstado = "<img class='imgTop' height='20' src='assets/images/disp.svg'><br><font size=1>Disp.</font>"
        // }else{
          obj.Estado = "<img class='imgTop' height='20' src='assets/images/disp.svg'> Disponível "
        // }
        
      }
      if (obj.comRestricao){
        // obj.Estado = "<font size="+fontSize+"px>Manutenção</font>"
        obj.Estado = "<img class='imgTop' height='20' src='assets/images/manut.svg'> Com Restrição"
        obj.iconeEstado = "<img class='imgTop' height='20' src='assets/images/gerando.svg'><br><font size=1>Gerando</font>"
      }
         // obj.Estado = "<font size="+fontSize+"px>Disponível</font>"
      
      this.prelista[id] = obj;
    }

    keys = Object.keys(this.prelista);
    this.lista = [];
    this.motores = [];
    for (var i = 0; i < keys.length; i++) {
      this.lista.push(this.prelista[keys[i]]);
      this.motores.push(this.prelista[keys[i]].nome);
    }
    this.source.load(this.lista);
    //this.settings.columns.nome.filter.config.completer.data= this.lista 
    //    this.source.setSort([{ field: 'data', direction: 'desc' }]);
  }
  motores=[];

  isMobile=false;
  calcWidth(){

    let w=window;
    let d=document;
    let e=d.documentElement;
    let g=d.getElementsByTagName('body')[0];
    let x = w.innerWidth||e.clientWidth||g.clientWidth;
    let y=w.innerHeight||e.clientHeight||g.clientHeight;
    //console.log("x "+x)
    // console.log("y "+y)

    if(x<600){
      this.isMobile = true;
    }else{
      this.isMobile = false;
    }

    return x;
  }

  onUserRowSelect(event): void {
    console.log("clicou");
    console.log(event);

    this.pnSelected = event.data.id
    this.servMotor.pottot=2.17;
    if(event.data.id.includes(".W")){
      this.servMotor.pottot=9.3;
    }
    if(event.data.id.includes("GI.") 
        && 
        (  
          event.data.id.includes("M17")  ||
          event.data.id.includes("M18")  ||
          event.data.id.includes("M19")  ||
          event.data.id.includes("M20")  ||
          event.data.id.includes("M21")  ||
          event.data.id.includes("M22")  ||
          event.data.id.includes("M23")  ||
          event.data.id.includes("M24")  ||
          event.data.id.includes("M25")  ||
          event.data.id.includes("M26")  ||
          event.data.id.includes("M27")  ||
          event.data.id.includes("M28")  
        )
      ){
      this.servMotor.pottot=2.27;
    }

    if(event.data.id.includes("GII.") 
        && 
        (
          event.data.id.includes("M49")  ||
          event.data.id.includes("M50")  ||
          event.data.id.includes("M51")  ||
          event.data.id.includes("M52")  ||
          event.data.id.includes("M53")  ||
          event.data.id.includes("M54")  ||
          event.data.id.includes("M55")  ||
          event.data.id.includes("M56")  ||
          event.data.id.includes("M57")  ||
          event.data.id.includes("M58")  ||
          event.data.id.includes("M59")  ||
          event.data.id.includes("M60")
        )
      ){
      this.servMotor.pottot=2.27;
    }
    
    //console.log(event.data['attr.id']);
    // this.downloadFile("", "", event.data['attr.id']);
    //this.dl("","",event.data['attr.id']);
    this.conta();
    this.showStaticModal();
  }

  parar = false;
  ngOnDestroy(): void {
    this.parar=true;
    clearInterval(this.verificaConexao.bind(this));
    this.websocket.close(); 
    console.log("disparou fechar ws");

  }
  porPagina(n){
    this.settings.pager.perPage = n;
    this.source.setPaging(1,n,true);
  }
  showStaticModal() {
    const activeModal = this.modalService.open(ModalMotoresComponent, {
      size: 'lg',
      backdrop: 'static',
      container: 'nb-layout',
    });

    //activeModal.componentInstance.modalHeader = this.servMotor.v;
    activeModal.componentInstance.modalContent = `This is static modal, backdrop click
                                                    will not close it. Click × or confirmation button to close modal.`;
  }
}

class linha {
  public nome;
  public grupo;
  public potencia;
  public Indisp;
  public manut;
  public gerando;
  public Estado;
  public id;
  public comRestricao;
}
