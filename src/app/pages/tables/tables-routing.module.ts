import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TablesComponent } from './tables.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { MotoresTableComponent } from './motores/motores-table.component';
import { ModalMotoresComponent } from '../ui-features/modals/modal/modalMotores.component';

const routes: Routes = [{
  path: '',
  component: TablesComponent,
  children: [{
    path: 'reports',
    component: SmartTableComponent,
  },
  {
    path: 'motors',
    component: MotoresTableComponent,
  },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [
    ModalMotoresComponent,
  ],
})
export class TablesRoutingModule { }

export const routedComponents = [
  TablesComponent,
  SmartTableComponent,
  MotoresTableComponent,
  ModalMotoresComponent,
  // RoscaComponent
];
