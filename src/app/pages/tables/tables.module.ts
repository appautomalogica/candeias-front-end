import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule, routedComponents } from './tables-routing.module';
import { SmartTableService } from '../../@core/data/smart-table.service';
import { MotorRealTimeService } from '../ui-features/modals/modal/modalMotores.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { RoscaComponent } from '../dashboard/rosca/rosca.component';
// import { CandeiasModule } from '../candeias.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { RoscaComponent } from '../dashboard/rosca/rosca.component';
import { DashboardModule } from '../dashboard/dashboard.module';
// import { AutomalogicaModule } from '../automalogica/automalogica.module';

import { ChartModule } from 'angular2-chartjs';

@NgModule({
  imports: [
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,
    NgxChartsModule,
    // AutomalogicaModule,
    NgxEchartsModule,
    DashboardModule,
    ChartModule,
  ],
  declarations: [
    ...routedComponents,
    // RoscaComponent,
  ],
  providers: [
    SmartTableService,
    MotorRealTimeService,
  ],
})
export class TablesModule { }
