
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MotorRealTimeService } from './modalMotores.service';
import { Component, OnDestroy } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
import * as moment from 'moment';
import 'moment/locale/pt-br';
// import { RoscaComponent } from '../../../automalogica/rosca/rosca.component';

@Component({
  selector: 'ngx-modal',
  template: `
    <div class="modal-header">
   <!-- <span>{{ servMotores.v }}</span> -->
    <span>{{servMotores.grupo}} - {{servMotores.nome}} - {{servMotores.estado}}</span>
      <button class="close" aria-label="Close" (click)="closeModal()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">  

      <div class="row">
      
        <div class="col-lg-6">  
          <nb-card size="small" class="rosca-card">
            <nb-card-header>Medições</nb-card-header>
            <nb-card-body>  
              <div style="text-align:center;margin:0 auto;width:200px;height:85%">
                <ngx-rosca style="text-align:center;margin:0 auto;width:180px"
                  [valor]="servMotores.v/1000" 
                  [total]="servMotores.pottot" 
                  altura=210 
                  largura=225  
                  translateX=30 
                  translateY=0
                  escala="2.1" 
                  [detalhes]="false" 
                  title="Potência">
                </ngx-rosca>
              </div>
              Horímetro: {{servMotores.horimetro}} horas
            </nb-card-body>
          </nb-card>
        </div>

        <!--
        <div class="col-lg-6">
          <nb-card size="small" class="barra-card">
            <nb-card-header>Disponibilidade</nb-card-header>
            <nb-card-body>
              <div style="position: absolute;top: 0px;left: 20px;">
                <ngx-charts-pie-chart [view]="viewPie" [tooltipText]="tooltipTextPie" [scheme]="colorSchemePie" [results]="resultsPie" [legend]="showLegendPie" [labels]="showLabelsPie"></ngx-charts-pie-chart>
              </div>
            </nb-card-body>
          </nb-card>
        </div>
        -->

      </div>
<!--
      <chart type="bar" [data]="dataChartBar" [options]="optionsChartBar"></chart>
-->

<div class="col-lg-12">
<nb-card size="medium" class="barra-card">
  <nb-card-header>Histórico de potência gerada</nb-card-header>
  <nb-card-body>
  <div style="width:98%;height:100%">
    <ngx-charts-line-chart
      [results]="dadosGrafPotMotor"
      [scheme]="colorSchemeLine"[xAxis]="showXAxisLine" [yAxis]="showYAxisLine" 
      [legend]="showLegendLine" [animations] = "animations" 
      [showXAxisLabel]="showXAxisLabelLine" [showYAxisLabel]="showYAxisLabelLine" 
      [xAxisLabel]="xAxisLabelLine" [yAxisLabel]="yAxisLabelLine"
      [timeline]="timeline" [autoScale]="autoScale" [roundDomains]="roundDomains" 
      [legendTitle]="Legenda" [xAxisTickFormatting]="xAxisTickFormatting">
      <ng-template #tooltipTemplate let-model="model">
          {{getTTData(model)}}
          <br>
          {{getTTNome(model)}}: <b> {{getTTValor(model)}} kW</b>
      </ng-template>
    </ngx-charts-line-chart> 
    </div>
  </nb-card-body>
</nb-card>
</div>


<div class="col-lg-12">
<nb-card size="medium" class="barra-card">
  <nb-card-header>Histórico de estados</nb-card-header>
  <nb-card-body>
  <div style="width:98%;height:65%">
    <ngx-charts-line-chart
      [results]="dadosGrafDisp"
      [scheme]="colorSchemeLineDisp"[xAxis]="showXAxisLine" [yAxis]="showYAxisLine" 
      [legend]="showLegendLineDisp" [animations] = "animations" 
      [showXAxisLabel]="showXAxisLabelLine" [showYAxisLabel]="showYAxisLabelLine" 
      [xAxisLabel]="xAxisLabelLine" [yAxisLabel]="yAxisLabelLine"
      [timeline]="timeline" [autoScale]="autoScale" [roundDomains]="roundDomains" 
      [legendTitle]="Legenda" [xAxisTickFormatting]="xAxisTickFormatting">
      <ng-template #tooltipTemplate let-model="model">
          {{getTTData(model)}}
          <br>
          {{getTTNome(model)}}: <b> {{getTTValor(model)}} </b>
      </ng-template>
    </ngx-charts-line-chart> 
    </div>
  </nb-card-body>
</nb-card>
</div>


      
    `,
})
export class ModalMotoresComponent {

// ///////////MODAL ORIGINAL////////////////////////////////////////
modalHeader: string;
modalContent = `Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
  nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
  nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.`;

  ////// ----  GRAFICO LINHA Potência
  Legenda = "Legenda"; 
  dadosGrafPotMotor = [
    {
      name: 'Motor',
      series: [
        {
          name: ' ',
          value: 0,
        },
      ],
    },
  ];
  dadosGrafDisp = [
    {
      name: 'v1',
      series: [
        {
          name: ' ',
          value: 0,
        },
      ],
    },
    {
      name: 'v2',
      series: [
        {
          name: ' ',
          value: 0,
        },
      ],
    },
    {
      name: 'v3',
      series: [
        {
          name: ' ',
          value: 0,
        },
      ],
    },
    {
      name: 'v4',
      series: [
        {
          name: ' ',
          value: 0,
        },
      ],
    },
    
  ];

  themeSubscriptionLine: any;
  themeSubscriptionLineDisp: any;
  showLegendLine = false;
  showLegendLineDisp = true;
  showXAxisLine = true;
  showYAxisLine = true;
  showXAxisLabelLine = false;
  xAxisLabelLine = 'Potência';
  showYAxisLabelLine = false;
  yAxisLabelLine = '';
  colorSchemeLine: any;
  colorSchemeLineDisp: any;
  animations = true;
  timeline = true;
  autoScale = true;
  roundDomains = true;
  xAxisTickFormatting = (cell => moment(cell).format('D/M, H[h]'));

  getTTData(i){
    return moment(i.name).format('D/M, H[h]')
  }

  getTTValor(i){
    return  i.value
  }

  getTTNome(i){
    return  i.series
    }
// --------------GRAFICO PIZZA----------------

  resultsPie = [
    { name: 'Gerando', value: 20 },
    { name: 'Disponível', value: 30 },
    { name: 'Indisponível', value: 20 },
    { name: 'Sem comunicação', value: 15 },
    { name: 'Manutenção', value: 15 },
  ];
  viewPie: number[] = [320, 270];
  showLegendPie = false;
  showLabelsPie = true;
  colorSchemePie: any;
  themeSubscriptionPie: any;

/////////////// GRAFICO BARRA

  themeSubscriptionChartBar: any;
  dataChartBar: any;
  optionsChartBar: any;

  constructor(
    public theme: NbThemeService,
    public activeModal: NgbActiveModal,
    public servMotores: MotorRealTimeService,
  ) {

    let umMesEmHoras = 30 * 24;
    /// GRAFICO Potência
    let pn;
    let Vpn = this.servMotores.id.split(".") ;
    pn = Vpn[0] + "_" + Vpn[1] + "_" + Vpn[2]
    pn = pn + '_DJ_PotAt';
    // console.log("-------------");
    // console.log("pn");
    // console.log(pn);
    // console.log("-------------");
    this.servMotores.getValoresEpmRest(pn, umMesEmHoras).subscribe(
      data => {
        this.dadosGrafPotMotor = this.servMotores.processaValoresRecebidosSQLparaGraficosNovo(data);
        this.dadosGrafPotMotor = [...this.dadosGrafPotMotor];
      },
      err => console.log(err)
    );

    this.themeSubscriptionLine = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorSchemeLine = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });

    /// GRAFICO Disp / estados
    Vpn = this.servMotores.id.split(".") ;
    pn = Vpn[0] + "_" + Vpn[1] + "_" + Vpn[2]
    let pnEstados = pn+'_DJ_Msinc,'+ pn+'_RST_Indisp,'+ pn+'_RST_Manut,'+ pn+'_RST_MotRst';
    // console.log("-------------");
    // console.log("pnEstados");
    // console.log(pnEstados);
    // console.log("-------------");
    this.servMotores.getValoresEpmRest(pnEstados, umMesEmHoras).subscribe(
      data => {
        let tempV = this.servMotores.processaValoresRecebidosSQLparaGraficosNovo(data);
        this.dadosGrafDisp = [...tempV];
      },
      err => console.log(err)
    );

    this.themeSubscriptionLineDisp = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorSchemeLineDisp = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });

    /// GRAFICO BARRA
    this.themeSubscriptionChartBar = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.dataChartBar = {
        labels: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
        datasets: [{
          data: [65, 59, 80, 81, 56, 55, 40],
          label: 'Series A',
          backgroundColor: NbColorHelper.hexToRgbA(colors.primaryLight, 0.8),
        }, {
          data: [28, 48, 40, 19, 86, 27, 90],
          label: 'Series B',
          backgroundColor: NbColorHelper.hexToRgbA(colors.infoLight, 0.8),
        }],
      };

      this.optionsChartBar = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: false,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
      };
    });
////////////////////////////////////\
    this.themeSubscriptionPie = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorSchemePie = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }

  tooltipTextPie = (cell => cell.value + "%");

  closeModal() {
    this.activeModal.close();
  }

  ngOnDestroy(): void {
    this.themeSubscriptionChartBar.unsubscribe();
    this.themeSubscriptionPie.unsubscribe();
    this.themeSubscriptionLine.unsubscribe();
    this.themeSubscriptionLineDisp.unsubscribe();
  }
}
