import { NgModule }      from '@angular/core';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { isNull } from 'util';

@Component({
  styleUrls: ['./login.scss'],
  template: `
  <div style="height=100%">
    <div class="grid">
      <div id="login">
  
        <h2><span class="fontawesome-lock"></span>Login </h2>
        
        <form action="#" method="POST">

          <fieldset>

            <p><label for="email">Usuário</label></p>
            <p><input [value]="username" (input)="username=$event.target.value" type="email" id="email" placeholder=" "></p>
  
            <p><label for="password">Senha</label></p>
            <p><input [value]="password" (input)="password=$event.target.value" type="password" id="password" placeholder=" "></p>
  
            <p><input type="submit" (click)="entrar()" value="Entrar"></p>
  
          </fieldset>
  
        </form>

      </div> <!-- end login -->
      <br>
      <div *ngIf="erro" align=center style="padding: 10px; border-radius: 25px; color: #D8000C;background-color: #FFD2D2;">
        <font><b>ERRO NO LOGIN!</b></font>
      </div>
    </div>
  </div>
  	`,
})
export class LoginComponent {

  constructor(
    public router: Router) {
      localStorage.clear();

      router.events.subscribe((val) => {
        // see also 
        // console.log("val instanceof LoginComponent") 
        // console.log(val instanceof LoginComponent) 
        this.erro = JSON.parse(localStorage.getItem('candeiasErro'));
        if(isNull(this.erro)){
          this.erro = false;
        }
        console.log("erro");
        console.log(this.erro);
    });

     
    //  localStorage.setItem('candeias', JSON.stringify({'user':this.username,'pass':this.password}));

  }
  erro = false;
  username = "";
  password = "";
entrar(){

  this.password =  CryptoJS.MD5(this.password)+'';
  // console.log("h:: "+ this.password);
  localStorage.setItem('candeias', JSON.stringify({'user':this.username,'pass':this.password}));
  //console.log('saved' + this.username)
  
  setTimeout(() => {
    this.router.navigate(['/pages/dashboard'])  
  },200);
  
}

}
