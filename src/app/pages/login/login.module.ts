import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { LoginComponent } from './login.component';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    // LoginComponent
  ],
  providers: [
  ],
})
export class LoginModule { }
