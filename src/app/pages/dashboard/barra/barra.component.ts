import { AfterViewInit, Component, Input, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-barra',
  styleUrls: ['./barra.component.scss'],
  template: `
  <!-- 
  <svg style=" position:absolute; top:0px; left:0px" width=100% height=100%>
  -->
  <svg width=100% height=23px>
  <g attr.transform='translate({{translateX}},{{translateY}}) scale(1)'>
            <text attr.x="{{showV1 ? x_in-12 :  x_in-3}}%" y=41 attr.fill="{{nomeCor}}" style="font: bold 14px Arial" text-anchor="end">
              {{ nome1 }}
            </text>
            <text attr.x="{{x_in-2}}%" y=41 attr.fill="{{cor1}}" style="font: bold 14px Arial" text-anchor="end">
              {{ showV1 ? v1 : '' }}
            </text>
            <rect
            y=28
            attr.x="{{x_in}}%"
            attr.width="{{((v1)/(total))*(x_fim-x_in) || 0 | number:0}}%"
            height="16" attr.fill="{{cor1}}" />
            <rect
            y=28
            attr.x="{{x_in+((v1)/(total))*(x_fim-x_in) || 0 | number:0}}%"
            attr.width="{{((v2)/(total))*(x_fim-x_in) || 0 | number:0}}%"
            height="16" attr.fill="{{cor2}}" />
            
            <text *ngIf="v2>0" attr.x="{{x_in+2+((((v1)+(v2))/(total))*(x_fim-x_in)) || x_fim+2}}%" y=41 attr.fill="{{v2>0 ? cor2 : cor }}" style="font: bold 14px Arial" text-anchor="start">
              {{ v2 }}{{ porcentagemV2 ? ("% ") : ("   ") }}  
            </text>

            <text *ngIf="nome2!='' " attr.x="{{x_fim+7}}%" y=41 attr.fill="{{v2>0 ? cor2 : cor1}}" style="font: bold 14px Arial" text-anchor="start">
              {{ nome2 }}  
            </text>
          </g>
        </svg>
  `,
})
export class BarraComponent implements AfterViewInit, OnDestroy {

  x_in: number = 47;
  x_fim: number = 85;
  linha : number = 0;
  nome1 : string = "";
  nomeCor : string = "#999999";
  nome2 : string = "";
  cor1 : string = "#999999";
  cor2 : string = "#ff0000";
  v1 : number = 0;
  showV1 : boolean = true;
  v2 : number = null;
  porcentagemV2 : boolean = false;
  total : number = 0;
  translateX : number = 10; 
  translateY : number = -25; 
  
  @Input('nome1')
  set name1Value(v: string) {
    this.nome1 = v;
  }
  @Input('nome2')
  set name2Value(v: string) {
    if(this.calcWidth()>600){
      this.nome2 = v;
      this.x_in = 35;
      this.x_fim = 62;
    }
  }

  @Input('linha')
  set linhaValue(v: number) {
    this.linha = v;
  }

  @Input('v1')
  set v1Value(v: number) {
    this.v1 = v;
  }

  @Input('showV1')
  set v1sValue(v: boolean) {
    this.showV1 = v;
    this.x_in = 28;
  }
  @Input('porcentagemV2')
  set porcentagemV2s(v: boolean) {
    this.porcentagemV2 = v;
  }

  @Input('v2')
  set v21Value(v: number) {
    this.v2 = v;
  }

  @Input('cor1')
  set cor1Value(v: string) {
    this.cor1 = v;
  }
  @Input('cor2')
  set cor2Value(v: string) {
    this.cor2 = v;
  }
  @Input('nomeCor')
  set nomeCorValue(v: string) {
    this.nomeCor = v;
  }

  @Input('total')
  set vTot(v: number) {
    this.total = v;
  }

  constructor(private theme: NbThemeService) {
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  formataPotencias3caracteres(pot: any, escala?: number){
    if(pot!==undefined){
      pot = pot +0;
      if(escala){
        pot= pot*escala;
      }
      if(Math.abs(pot)>100){
        pot = Math.round(pot)
      }else if(Math.abs(pot)>10){
        pot = Math.round(pot*10)/10;
      }else {
        pot = Math.round(pot*100)/100;
      }
      pot=pot+"";
      if(pot.length===1 && !(pot.includes("."))){
        pot = pot + ".00"
      }
      if(pot.length===2 && !(pot.includes("."))){
        pot = pot + ".0"
      }
      if(pot.length===3 && pot.includes(".")){
        pot = pot + "0"
      }
      return pot;
    }else{
      return 0;
    }
  }
  calcWidth(){

    let w=window;
    let d=document;
    let e=d.documentElement;
    let g=d.getElementsByTagName('body')[0];
    let x = w.innerWidth||e.clientWidth||g.clientWidth;
    let y=w.innerHeight||e.clientHeight||g.clientHeight;
    // console.log("x "+x)
    // console.log("y "+y)
    return x;
  }
  
}
