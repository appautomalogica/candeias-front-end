import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { StatusCardComponent } from './status-card/status-card.component';
import { ContactsComponent } from './contacts/contacts.component';
import { RoomsComponent } from './rooms/rooms.component';
import { RoomSelectorComponent } from './rooms/room-selector/room-selector.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { TemperatureDraggerComponent } from './temperature/temperature-dragger/temperature-dragger.component';
import { TeamComponent } from './team/team.component';
import { KittenComponent } from './kitten/kitten.component';
import { SecurityCamerasComponent } from './security-cameras/security-cameras.component';
import { ElectricityComponent } from './electricity/electricity.component';
import { ElectricityChartComponent } from './electricity/electricity-chart/electricity-chart.component';
import { WeatherComponent } from './weather/weather.component';
import { SolarComponent } from './solar/solar.component';
import { GenericComponent } from './generic/generic.component';
import { PlayerComponent } from './rooms/player/player.component';
import { TrafficComponent } from './traffic/traffic.component';
import { TrafficChartComponent } from './traffic/traffic-chart.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { BarraComponent } from './barra/barra.component';
import { RoscaComponent } from './rosca/rosca.component';
import { ChartModule } from 'angular2-chartjs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NotificacoesComponent } from './notificacoes.component';
import { Global1Component } from './global1.component';
import { Global2Component } from './global2.component';
import { CadastroUsuariosComponent } from './cadastro-usuarios.component';
// import { CandeiasModule } from '../candeias.module';
// import { AutomalogicaModule } from '../automalogica/automalogica.module';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    NgxChartsModule,
    // AutomalogicaModule
    ChartModule,
  ],
  exports: [ RoscaComponent ],
  declarations: [
    DashboardComponent,
    NotificacoesComponent,
    Global1Component,
    Global2Component,
    StatusCardComponent,
    TemperatureDraggerComponent,
    ContactsComponent,
    RoomSelectorComponent,
    TemperatureComponent,
    RoomsComponent,
    TeamComponent,
    KittenComponent,
    SecurityCamerasComponent,
    ElectricityComponent,
    ElectricityChartComponent,
    WeatherComponent,
    PlayerComponent,
    SolarComponent,
    GenericComponent,
    BarraComponent,
    RoscaComponent,
    TrafficComponent,
    TrafficChartComponent,
    CadastroUsuariosComponent,
  ],
})
export class DashboardModule { }
