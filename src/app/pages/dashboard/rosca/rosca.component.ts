import { AfterViewInit, Component, Input, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

declare const echarts: any;
@Component({
  selector: 'ngx-rosca',
  styleUrls: ['./rosca.component.scss'],
  template: `
        <!-- ____________________________________________________________________ -->
        <svg style="display: inline-block; vertical-align: middle;" attr.width={{largura}} attr.height={{altura}}>
        <g attr.transform='translate({{translateX}},{{translateY}}) scale({{escala}})'>
          <circle cx=35 cy=40 r=31 stroke="black" stroke-width="0" fill="#d2d2d2" />
        
            <path fill="none" stroke="#0098F1" stroke-width="18" attr.d="{{describeArc(35, 40, 22, 0, (valor/total)*360 )}}" />
            <path fill="none" stroke="#FF0000" stroke-width="18" attr.d="{{describeArc(35, 40, 22, (valor/total)*360 , ((inef+valor)/total)*360 )}}" />
            <path fill="none" stroke="#FFFF00" stroke-width="18" attr.d="{{describeArc(35, 40, 22, ((inef+valor)/total)*360 ,((inef+indisp+valor)/total)*360)}}" />
        
            <circle cx=35 cy=40 r=22 stroke="black" stroke-width="0" fill="#FFFFFF" />
            <text text-anchor="middle" x=35 y=48 fill="#0098F1" style="font: bold 19px Arial">
              {{ formataPotencias3caracteres(valor) }}
            </text>
            <text text-anchor="middle" x=44 y=80 fill="#d2d2d2" style="font: bold 9px Arial">
              {{ total }} MW
            </text>

            <g *ngIf="detalhes">
              <g *ngIf="direita" transform='translate(0,-10)'>

                <g *ngIf="direita">
                    <text x=98 y=31 fill="#787878" style="font: bold 8px Arial" text-anchor="end">
                      Ativa
                    </text>
                    <text x=140 y=32 fill="#0098F1" style="font: bold 16px Arial" text-anchor="end">
                      {{ formataPotencias4caracteres(valor) }}
                    </text>
                    <text x=142 y=31 fill="#787878" style="font: bold 8px Arial" text-anchor="start">
                      MW
                    </text>

                    <text x=98 y=85 fill="#787878" style="font: bold 8px Arial" text-anchor="end">
                      Ociosa
                    </text>
                    <text x=140 y=86 fill="#d2d2d2" style="font: bold 16px Arial" text-anchor="end">
                      {{ calcOciosa()  }}
                    </text>
                    <text x=142 y=85 fill="#787878" style="font: bold 8px Arial" text-anchor="start">
                      MW
                    </text>
                </g>

                <text x=98 y=49 fill="#787878" style="font: bold 8px Arial" text-anchor="end">
                  Inef.
                </text>
                <text x=140 y=50 fill="#FF0000" style="font: bold 16px Arial" text-anchor="end">
                  {{ formataPotencias4caracteres(inef) }}
                </text>
                <text x=142 y=49 fill="#787878" style="font: bold 8px Arial" text-anchor="start">
                  MW
                </text>
                <text x=98 y=67 fill="#787878" style="font: bold 8px Arial" text-anchor="end">
                  Indisp.
                </text>
                <text x=140 y=68 fill="#C9BF00" style="font: bold 16px Arial" text-anchor="end">
                  {{ formataPotencias4caracteres(indisp) }}
                </text>
                <text x=142 y=67 fill="#787878" style="font: bold 8px Arial" text-anchor="start">
                  MW
                </text>
              </g>
            </g>
          </g>
        </svg>
  `,
})
export class RoscaComponent implements AfterViewInit, OnDestroy {

  valor: number = null;
  title: string = "";
  total: number = null;
  inef: number = null;
  indisp: number = null;
  ociosa: number = null;
  detalhes: boolean = true;
  direita: boolean = false;
  escala: number = 1.5;
  altura: number = 111;
  largura: number = 250;
  translateX: number = 0;
  translateY: number = -10;

  @Input('valor')
  set chartValue(v: number) {
    this.valor = v;
  }

  @Input('detalhes')
  set setdetalhes(v: boolean) {
    this.detalhes = v;
  }

  @Input('direita')
  set setdireita(v: boolean) {
    this.direita = v;
  }

  @Input('escala')
  set setescala(v: number) {
    this.escala = v;
  }

  @Input('altura')
  set setaltura(v: number) {
    this.altura = v;
  }
  @Input('largura')
  set setlargura(v: number) {
    this.largura = v;
  }

  @Input('translateX')
  set settranslateX(v: number) {
    this.translateX = v;
  }

  @Input('translateY')
  set settranslateY(v: number) {
    this.translateY = v;
  }

  @Input('title')
  set titleValue(value: string) {
    this.title = value;
  }

  @Input('total')
  set valorValue(v: number) {
    this.total = v;
  }

  @Input('inef')
  set inefValue(v: number) {
    this.inef = v;
  }

  @Input('indisp')
  set indispValue(v: number) {
    this.indisp = v;
  }

  // @Input('ociosa')
  // set ociosaValue(v: number) {
  //   this.ociosa = v;
  // }

  option: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService) {
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  calcOciosa(){
    // console.log("inef");
    // console.log(this.inef);
    // console.log("valor");
    // console.log(this.valor);
    // console.log("indisp");
    // console.log(this.indisp);
    // console.log("total");
    // console.log(this.total);
    let v= this.total-this.inef-this.valor-this.indisp;
    // console.log(v);
    let r = this.formataPotencias4caracteres(v)
    // console.log("r");
    // console.log(r);
    return  r;
  }

  formataPotencias3caracteres(pot: any, escala?: number) {
    if (pot !== undefined) {
        pot = pot + 0;
        if (escala) {
          pot = pot * escala;
        }
        if (Math.abs(pot) > 100) {
          pot = Math.round(pot)
        } else if (Math.abs(pot) > 10) {
          pot = Math.round(pot * 10) / 10;
        } else {
          pot = Math.round(pot * 100) / 100;
        }
        pot = pot + "";
        if (pot.length === 1 && !(pot.includes("."))) {
          pot = pot + ".00"
        }
        if (pot.length === 2 && !(pot.includes("."))) {
          pot = pot + ".0"
        }
        if (pot.length === 3 && pot.includes(".")) {
          // console.log("v")
          // console.log(pot)
          pot = pot + "0"
        }
        // console.log(pot)
        if (isNaN(pot)) {
          return 0;
        }
        return pot;
    } else {
      return 0;
    }
  }

  formataPotencias4caracteres(pot: any, escala?: number) {
    if (pot !== undefined) {
        pot = pot + 0;
        if (escala) {
          pot = pot * escala;
        }
        if (Math.abs(pot) > 1000) {
          pot = Math.round(pot)
        } else if (Math.abs(pot) > 10) {
          pot = Math.round(pot * 10) / 10;
        } else if (Math.abs(pot) > 100) {
          pot = Math.round(pot * 100) / 100;
        } else {
          pot = Math.round(pot * 1000) / 1000;
        }
        pot = pot + "";
        if (pot.length === 1 && !(pot.includes("."))) {
          pot = pot + ".000"
        }
        if (pot.length === 2 && !(pot.includes("."))) {
          pot = pot + ".00"
        }
        if (pot.length === 3 && pot.includes(".")) {
          // console.log("v")
          // console.log(pot)
          pot = pot + "00"
        }
        if (pot.length === 4 && pot.includes(".")) {
          // console.log("v")
          // console.log(pot)
          pot = pot + "0"
        }
        // console.log(pot)
        if (isNaN(pot)) {
          return 0;
        }
        return pot;
    } else {
      return 0;
    }
  }



  polarToCartesian(centerX, centerY, radius, angleInDegrees): any {
    var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

    return {
      x: centerX + (radius * Math.cos(angleInRadians)),
      y: centerY + (radius * Math.sin(angleInRadians))
    };
  }

  describeArc(x, y, radius, startAngle, endAngle) {
    let d = "";
    if (startAngle < endAngle) {
      let delta = endAngle - startAngle;
      if (delta > 360) {
        startAngle = 0;
        endAngle = 359.99;
      }
      while (startAngle > 360) {
        startAngle = startAngle - 360;
      }
      while (endAngle > 360) {
        endAngle = endAngle - 360;
      }
      let start;
      let end;
      let largeArcFlag;
      if (startAngle < endAngle) {
        start = this.polarToCartesian(x, y, radius, endAngle);
        end = this.polarToCartesian(x, y, radius, startAngle);
        largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";
        d = [
          "M", start.x, start.y,
          "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");
      } else {
        let endAngleM = 359.99;
        start = this.polarToCartesian(x, y, radius, endAngleM);
        end = this.polarToCartesian(x, y, radius, startAngle);
        largeArcFlag = endAngleM - startAngle <= 180 ? "0" : "1";
        d = [
          "M", start.x, start.y,
          "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");

        let startAngleM = 0;
        start = this.polarToCartesian(x, y, radius, endAngle);
        end = this.polarToCartesian(x, y, radius, startAngleM);
        largeArcFlag = endAngle - startAngleM <= 180 ? "0" : "1";
        d = [
          d,
          "M", start.x, start.y,
          "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");
      }

    }
    return d;
  }
}
