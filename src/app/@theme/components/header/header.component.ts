import { AuthGuard } from '../../../auth-guard.service';
import { Component, Input, OnInit } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { Router } from '@angular/router';


@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {


  @Input() position = 'normal';

  user: any;

  userMenu = [{ title: 'Profile' }, { title: 'Log out' }];
  nomeUsuario: string = '';

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              public router : Router,
              private userService: UserService,
              private analyticsService: AnalyticsService,
              public service: AuthGuard,
            ) {
  }

  ngOnInit() {
    // this.userService.getUsers()
    //   .subscribe((users: any) => this.user = users.nick);
    this.nomeUsuario = this.service.nomeUser;
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  // toggleSettings(): boolean {
  //   this.sidebarService.toggle(false, 'settings-sidebar');
  //   return false;
  // }

  goToHome() {
    this.menuService.navigateHome();
  }

  sair(){
    localStorage.removeItem('candeias');
    this.router.navigate(['/auth'])
  }

  // startSearch() {
  //   this.analyticsService.trackEvent('startSearch');
  // }
}
