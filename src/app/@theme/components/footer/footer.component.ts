import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by"> </span>
    <div class="socials">
      <a href="#" target="_blank" >
      <img class="imgTop" height="30" src="assets/images/automalogica-sem-borda.svg">
      <!--
      <img class="imgTop" height="30" src="../../../../assets/images/automalogica-degrade1.svg">
      <img class="imgTop" height="30" src="../../../../assets/images/automalogica-degrade2.svg">
      <img class="imgTop" height="30" src="../../../../assets/images/automalogica-degrade3.svg">
      <img class="imgTop" height="30" src="../../../../assets/images/automalogica-degrade4.svg">
      <img class="imgTop" height="30" src="../../../../assets/images/automalogica-degrade5.svg">
      -->
      </a>
    </div>
  `,
})
export class FooterComponent {
}
